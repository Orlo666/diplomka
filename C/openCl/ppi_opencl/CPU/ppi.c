
/* Include the clBLAS header. It includes the appropriate OpenCL headers */

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <gsl/gsl_sort.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_sort.h"
#include "gsl/gsl_sort_vector.h"
#include <matlab_io.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>
#include <clBLAS.h>
#include <sys/time.h>

#include "err_code.h"

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

extern int output_device_info(cl_device_id );
extern double wtime();



char *getKernelSource(char *filename);

int ppi(gsl_matrix **out_matrix, gsl_matrix *matrix, int q, int iter) {
    double start_time;
    double run_time;
    start_time = wtime();
    int orig_rows = matrix->size1;
    int orig_cols = matrix->size2;

    gsl_matrix* copy_matrix = gsl_matrix_alloc(orig_rows, orig_cols);
    gsl_matrix* skewers = gsl_matrix_alloc(orig_rows, iter);

    
    double* votes = malloc(sizeof(double) * orig_cols);
    for (int i=0; i<orig_cols; i++) {
        votes[i] = 0.0;
    }
    gsl_matrix_memcpy(copy_matrix, matrix);

    char *max_indexes_kernel = getKernelSource("./max_indexes.cl");
    // removing mean from data
    //


    double mean = 0.0;
    for (int i=0; i<orig_rows; i++) {
        for (int j=0; j<orig_cols; j++) {
            mean += gsl_matrix_get(copy_matrix, i, j);
        }
        mean /= orig_cols;
        for (int j=0; j<orig_cols; j++) {
            gsl_matrix_set(copy_matrix, i, j, gsl_matrix_get(copy_matrix, i, j) - mean);
        }
        mean = 0.0;
    }

    run_time = start_time - wtime();
    puts("init");
    printf("%f s\n", run_time);
    start_time = wtime();
    const gsl_rng_type * T;
    gsl_rng * r;
    double mu = 1;

    gsl_rng_env_setup();
    struct timeval tv; // Seed generation based on time
    gettimeofday(&tv,0);
    unsigned long mySeed = tv.tv_sec + tv.tv_usec;
    T = gsl_rng_default; // Generator setup
    r = gsl_rng_alloc (T);
    gsl_rng_set(r, mySeed);

    for (int i=0; i<orig_rows; i++) {
        for (int j=0; j<iter; j++) {
            double k = gsl_ran_gaussian (r, mu);
            gsl_matrix_set(skewers, i, j, k);
        }
    }

    run_time = wtime() - start_time;
    puts("generating rand skewers");
    printf("%f s\n", run_time);
    gsl_rng_free (r);

    gsl_vector_view vector_view;

    int *end_members_indexes = malloc(sizeof(int) * q * 2);
    int index = 0;

    // clBLAS init
    clblasOrder order = clblasRowMajor;

    // init OPENCL
    cl_device_id     device_id;     // compute device id
    cl_context       ctx;       // compute context
    cl_command_queue queue;      // compute command queue
    cl_program       program;       // compute program
    cl_kernel        kernel;
        
    cl_mem bufA, bufX, bufY, bufD;
    cl_int err;
    cl_event event = NULL;
    int ret = 0;

    // Fill vectors a and b with random float values

    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkError(err, "Finding platforms");
    if (numPlatforms == 0)
    {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++)
    {
        err = clGetDeviceIDs(Platform[i], CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
        if (err == CL_SUCCESS)
        {
            break;
        }
    }

    if (device_id == NULL)
        checkError(err, "Finding a device");

    err = output_device_info(device_id);
    checkError(err, "Printing device output");

    // Create a compute context
    ctx = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    checkError(err, "Creating context");

    // Create a command queue
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");


    /**************************************
    * clBLAS
    *************************************/
    // clBLAS Setup
    err = clblasSetup();
    if (err != CL_SUCCESS) {
        printf("clblasSetup() failed with %d\n", err);
        clReleaseCommandQueue(queue);
        clReleaseContext(ctx);
        return 1;
    }


    double *result = malloc(sizeof(double) * orig_cols * iter);

    start_time = wtime();
    gsl_matrix *temp_matrix = gsl_matrix_alloc(orig_cols, iter);
    
    gsl_vector *vector = gsl_vector_calloc(orig_cols);

    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(copy_matrix->data),
                          NULL, &err);
    bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * iter * sizeof(skewers->data),
                          NULL, &err);
    bufY = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_cols * iter * sizeof(result),
                          NULL, &err);

    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
        orig_rows * orig_cols * sizeof(copy_matrix->data), copy_matrix->data, 0, NULL, NULL);

    err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
        orig_rows * iter * sizeof(skewers->data), skewers->data, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
        orig_cols * iter * sizeof(result), result, 0, NULL, NULL);
    err = clblasDgemm(order, clblasTrans, clblasNoTrans, orig_cols, iter, orig_rows,
                      1.0, bufA, 0, orig_cols,
                      bufX, 0, iter,
                      0.0, bufY, 0, iter, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufY, CL_TRUE, 0, orig_cols * iter * sizeof(result),
                                  result, 0, NULL, NULL);

    }
    run_time = wtime() -  start_time;
    printf("matrix multiplication...\n");
    printf("%f s\n", run_time);

    temp_matrix->data = result;



    clReleaseMemObject(bufA);
    clReleaseMemObject(bufX);
    clReleaseMemObject(bufY);

    clReleaseCommandQueue(queue);

/***************************************************
 * GET MAX IDX OF SKEWERS
 ***************************************************/
    start_time = wtime();

    cl_int * max_idx_array = malloc(iter * sizeof(cl_int));
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    program = clCreateProgramWithSource(ctx, 1, (const char **) & max_indexes_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "max_indexes", &err);
    checkError(err, "Creating kernel");

    bufD = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, orig_cols * iter * sizeof(double),
                          result, &err);
    checkError(err, "Creating buffer bufD");

    bufX = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, iter * sizeof(cl_int), max_idx_array, &err);
    checkError(err, "Creating buffer bufX");
        
    size_t global = iter;
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufD);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 1, sizeof(int), &orig_cols);
    checkError(err, "Setting kernel arguments");
err |= clSetKernelArg(kernel, 2, sizeof(int), &iter);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &bufX);
    checkError(err, "Setting kernel arguments");
    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, NULL,
        &global, NULL,
        0, NULL, NULL);

    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clEnqueueReadBuffer(queue, bufX, CL_TRUE, 0, iter*sizeof(cl_int),
                              max_idx_array, 0, NULL, NULL);
    checkError(err, "Getting max_idx back from kernel");
    
    for (int i=0; i<iter; i++) {
        votes[max_idx_array[i]] += 1.0;
    }

    run_time = wtime() -  start_time;
    printf("MAX IDX OF SKEWERS...\n");
    printf("%f s\n", run_time);


    /***************************************************
     * COMPUTING VOTES AND ENDMEMBERS
     ***************************************************/
    clReleaseMemObject(bufD);
    clReleaseMemObject(bufX);
    /* Release OpenCL events. */
    clReleaseEvent(event);
    /* Release OpenCL memory objects. */
    /* Finalize work with clblas. */
    clblasTeardown();
    /* Release OpenCL working objects. */
    clReleaseCommandQueue(queue);
    clReleaseContext(ctx);
    gsl_matrix_free(temp_matrix);


    int stride = 1;
    gsl_sort_largest_index (end_members_indexes, q, votes, stride, orig_cols);

    for (int i=0; i<orig_rows; i++){
        for (int j=0; j<q*2; j=j+1+stride) {
            gsl_matrix_set(*out_matrix, i, j/2, gsl_matrix_get(matrix, i, end_members_indexes[j]));
        }
    }

    gsl_matrix_free(copy_matrix);
    gsl_matrix_free(skewers);
    free(votes);
    free(end_members_indexes);
    free(max_idx_array);
    return;

    
}

int main () {
    char* end_members_file = "../../../../data/Data_MAT/end3.mat";
    char* original_file = "../../../../data/Data_MAT/samson_1.mat";
    char* covariance_file = "../../../../data/Data_MAT/covariance.mat";
    char* output_file = "../../../../data/Data_MAT/C_ppi.mat";

    mxArray *orig_pa = NULL;
    gsl_matrix *orig_matrix;
    gsl_matrix *output;
    int orig_cols;
    int orig_rows;
    int q = 3;
    int iter = 1000;
    double *orig_data;

    orig_pa = load_variable_from_file(original_file, "V");

    if (orig_pa == NULL) {
        printf("moje chyba v orig_pa\n");
        return 1;
    }

    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_cols = mxGetN(orig_pa);
    orig_matrix= gsl_matrix_alloc (orig_rows, orig_cols);

    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_cols; j++) {
            gsl_matrix_set(orig_matrix, i, j, orig_data[j*orig_rows + i]);
        }
    }


    output = gsl_matrix_alloc(orig_rows, q);
    ppi(&output, orig_matrix, q, iter);

    // one more context
    cl_platform_id platform = 0;
    cl_context ctx;       // compute context
    cl_device_id device_id;
    cl_int err;
    cl_uint numPlatforms;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };

    platform = 0;

    err = clGetPlatformIDs(1, &platform, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetPlatformIDs() failed with %d\n", err );
        return 1;
    }
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetDeviceIDs() failed with %d\n", err );
        return 1;
    }
    props[1] = (cl_context_properties)platform;


    ctx = clCreateContext(props, 1, &device_id, NULL, NULL, &err);
    clReleaseContext(ctx);


    save_matrix_to_file_gsl(output, output_file, "ppi_c");

    gsl_matrix_free(orig_matrix);
    gsl_matrix_free(output);
}


__kernel void max_indexes(
   __global double *vector,
   const int vector_size,
   const int cols,
   __global int *idx) {

    int k =0;
    int id_row = get_global_id(0);
    double max = -1.0;

    for (k=0; k<vector_size; k++) {
        if (fabs(vector[id_row + k*cols]) > max) {
            max = fabs(vector[id_row + k*cols]);
            idx[id_row] = k;
        }
    }
}

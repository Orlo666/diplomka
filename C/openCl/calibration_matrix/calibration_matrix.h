#ifndef CALIB_MAT
#define CALIB_MAT

#include <stdio.h>
#include <string.h>
#include <libxml/xmlreader.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

int create_calibration_matrix(char *filename_path, float *filter1, float *filter2);

#endif

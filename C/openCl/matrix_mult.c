
/* Include the clBLAS header. It includes the appropriate OpenCL headers */

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <gsl/gsl_sort.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_sort.h"
#include "gsl/gsl_sort_vector.h"
#include "../matlab_io.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>
#include <clBLAS.h>
#include <sys/time.h>

#include "err_code.h"

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

extern int output_device_info(cl_device_id );
extern double wtime();

int ppi(gsl_matrix **out_matrix, gsl_matrix *matrix, int q, int iter) {
    double start_time;
    double run_time;
    start_time = wtime();
    int orig_rows = matrix->size1;
    int orig_cols = matrix->size2;

    gsl_matrix* copy_matrix = gsl_matrix_alloc(orig_rows, orig_cols);
    gsl_matrix* skewers = gsl_matrix_alloc(orig_rows, iter);

    
    double* votes = malloc(sizeof(double) * orig_cols);
    gsl_matrix_memcpy(copy_matrix, matrix);


    // removing mean from data

    double mean = 0.0;
    for (int i=0; i<orig_rows; i++) {
        for (int j=0; j<orig_cols; j++) {
            mean += gsl_matrix_get(copy_matrix, i, j);
        }
        mean /= orig_cols;
        for (int j=0; j<orig_cols; j++) {
            gsl_matrix_set(copy_matrix, i, j, gsl_matrix_get(copy_matrix, i, j) - mean);
        }
        mean = 0.0;
    }

    run_time = start_time - wtime();
    puts("init");
    printf("%f s\n", run_time);
    start_time = wtime();
    const gsl_rng_type * T;
    gsl_rng * r;
    double mu = 1;

    gsl_rng_env_setup();
    struct timeval tv; // Seed generation based on time
    gettimeofday(&tv,0);
    unsigned long mySeed = tv.tv_sec + tv.tv_usec;
    T = gsl_rng_default; // Generator setup
    r = gsl_rng_alloc (T);
    gsl_rng_set(r, mySeed);

    for (int i=0; i<orig_rows; i++) {
        for (int j=0; j<iter; j++) {
            double k = gsl_ran_gaussian (r, mu);
            gsl_matrix_set(skewers, i, j, k);
        }
    }

    run_time = wtime() - start_time;
    puts("generating rand skewers");
    printf("%f s\n", run_time);
    gsl_rng_free (r);

    gsl_vector_view vector_view;

    int *end_members_indexes = malloc(sizeof(int) * q);
    int index = 0;
    for (int i=0; i<orig_cols; i++) {
        votes[i] = 0.0;
    }
    for (int i=4; i>0; i--) {
        printf("%d \n", end_members_indexes[i]);
    }

    // clBLAS init
    clblasOrder order = clblasRowMajor;

    // init OPENCL
    cl_device_id     device_id;     // compute device id
    cl_context       ctx;       // compute context
    cl_command_queue queue;      // compute command queue
    cl_program       program;       // compute program
    cl_kernel        ko_vadd;       // compute kernel

    cl_mem bufA, bufX, bufY;
    cl_int err;
    cl_event event = NULL;
    int ret = 0;

    // Fill vectors a and b with random float values

    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkError(err, "Finding platforms");
    if (numPlatforms == 0)
    {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++)
    {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS)
        {
            break;
        }
    }

    if (device_id == NULL)
        checkError(err, "Finding a device");

    err = output_device_info(device_id);
    checkError(err, "Printing device output");

    // Create a compute context
    ctx = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    checkError(err, "Creating context");

    // Create a command queue
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");


    /**************************************
    * clBLAS
    *************************************/
    // clBLAS Setup
    err = clblasSetup();
    if (err != CL_SUCCESS) {
        printf("clblasSetup() failed with %d\n", err);
        clReleaseCommandQueue(queue);
        clReleaseContext(ctx);
        return 1;
    }


    double *result = malloc(sizeof(double) * orig_cols);

    start_time = wtime();
    gsl_matrix *temp_matrix = gsl_matrix_alloc(orig_cols, iter);
    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(copy_matrix->data),
                          NULL, &err);
    
    for (int i=0; i<iter; i++) {
        gsl_vector *vector = gsl_vector_calloc(orig_cols);
        vector_view = gsl_matrix_column(skewers, i);


        bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * sizeof(*vector_view.vector.data),
                              NULL, &err);
        bufY = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_cols * sizeof(result),
                              NULL, &err);

        err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
            orig_rows * orig_cols * sizeof(copy_matrix->data), copy_matrix->data, 0, NULL, NULL);

        err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
            orig_rows * sizeof(*vector_view.vector.data), vector_view.vector.data, 0, NULL, NULL);
        err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
            orig_cols * sizeof(result), result, 0, NULL, NULL);
        err = clblasDgemv(order, clblasTrans, orig_rows, orig_cols, 1.0, bufA, 0, orig_cols, bufX, 0, 1, 0.0, bufY, 0, 1, 1, &queue, 0, NULL, &event);

        //err = gsl_blas_dgemv (CblasTrans, 1.0, copy_matrix, &vector_view.vector, 0.0, vector);

        if (err != CL_SUCCESS) {
            printf("clblasDgemv() failed with %d\n", err);
            printf("%d\n",i);
            

            ret = 1;
        }
        else {
            /* Wait for calculations to be finished. */
            err = clWaitForEvents(1, &event);
            /* Fetch results of calculations from GPU memory. */
            err = clEnqueueReadBuffer(queue, bufY, CL_TRUE, 0, orig_cols * sizeof(result),
                                      result, 0, NULL, NULL);
            vector->data = result;
            for(int j=0; j<orig_cols; j++) {
                gsl_vector_set(vector, j, fabs(gsl_vector_get(vector, j)));
            }

            index = gsl_vector_max_index(vector);

            votes[index]+= 1.0;
        }
        clReleaseMemObject(bufY);
        clReleaseMemObject(bufX);
    }

    /* Release OpenCL events. */
    clReleaseEvent(event);
    /* Release OpenCL memory objects. */
    /* Finalize work with clblas. */
    clblasTeardown();
    /* Release OpenCL working objects. */
    clReleaseCommandQueue(queue);
    clReleaseContext(ctx);
    gsl_matrix_free(temp_matrix);

    run_time = wtime() -  start_time;
    printf("%d iteration of matrix mult...\n", iter);
    printf("%f s\n", run_time);

    //gsl_sort_largest_index (end_members_indexes, 3, votes, 1, orig_cols);
    for (int i=0; i<q; i++) {
        printf("%d\n", end_members_indexes[i]);
    }
    
    int one = 0;
    int two = 0;
    int three = 0;
    double max = 0.0;

    for (int i=0; i<orig_cols; i++) {
        if (max < votes[i]) {
            max = votes[i];
            one = i;
        }
    }
    votes[one] = 0;
    max = 0;
    for (int i=0; i<orig_cols; i++) {
        if (max < votes[i]) {
            max = votes[i];
            two = i;
        }
    }
    votes[two] = 0;
    max = 0;
    for (int i=0; i<orig_cols; i++) {
        if (max < votes[i]) {
            max = votes[i];
            three = i;
        }
    }

    end_members_indexes[0] = one;
    end_members_indexes[1] = two;
    end_members_indexes[2] = three;



    for (int i=0; i<3; i++) {
        printf("%d ", end_members_indexes[i]);
    }
    printf("\n");

    for (int i=0; i<orig_rows; i++){
        for (int j=0; j<q; j++) {
            gsl_matrix_set(*out_matrix, i, j, gsl_matrix_get(matrix, i, end_members_indexes[j]));
        }
    }

    gsl_matrix_free(copy_matrix);
    gsl_matrix_free(skewers);
    free(votes);
    free(end_members_indexes);
    return;

    
}

int main () {
    char* end_members_file = "../../data/Data_MAT/end3.mat";
    char* original_file = "../../data/Data_MAT/samson_1.mat";
    char* covariance_file = "../../data/Data_MAT/covariance.mat";
    char* output_file = "../../data/Data_MAT/C_ppi.mat";

    mxArray *orig_pa = NULL;
    gsl_matrix *orig_matrix;
    gsl_matrix *output;
    int orig_cols;
    int orig_rows;
    int q = 3;
    int iter = 1000;
    double *orig_data;

    orig_pa = load_variable_from_file(original_file, "V");

    if (orig_pa == NULL) {
        printf("moje chyba v orig_pa\n");
        return 1;
    }

    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_cols = mxGetN(orig_pa);
    orig_matrix= gsl_matrix_alloc (orig_rows, orig_cols);

    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_cols; j++) {
            gsl_matrix_set(orig_matrix, i, j, orig_data[j*orig_rows + i]);
        }
    }


    output = gsl_matrix_alloc(orig_rows, q);
    ppi(&output, orig_matrix, q, iter);

    save_matrix_to_file(output, output_file, "ppi_c");

    gsl_matrix_free(orig_matrix);
    gsl_matrix_free(output);
}



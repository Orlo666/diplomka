#include <mex.h>
#include <pca.h>
#include <fcls.h>
#include <ppi.h>
#include <matlab_io.h>
#include <getopt.h>

/* Flag set by ‘--verbose’. */


#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif


static int verbose_flag;
extern int output_device_info(cl_device_id );
extern double wtime();

extern int non_constrained_simple(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols);

extern int isra(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols);

extern int sum_to_one(double *abundances, int abun_cols, int abun_rows);
extern int ppi(double *out_matrix, double *matrix, int orig_rows, int orig_cols, int q, int iter);
extern int pca(double *image,int orig_rows,int orig_cols, double **output, int *output_rows);
extern mxArray* load_variable_from_file(char *file_name, char *variable_name);
extern void save_matrix_to_file(double *matrix, int rows, int cols, char *file_name, char *variable_name);
extern int create_identity_matrix(double *matrix, int cols, int rows);
extern int qr_algorithm(double *matrix, int cols, int rows, double *eigen_values, double *eigen_vectors);



int main(int argc, char **argv) {
    // TODO use args
    //

    float whole_start_time = wtime();

    int c;
    char* output_file = NULL;
    char* original_file = NULL;
    while (1) {
        static struct option long_options[] =
            {
                /* These options set a flag. */
                {"verbose", no_argument, &verbose_flag, 1},
                {"brief", no_argument, &verbose_flag, 0},
                /* These options don’t set a flag.
                   We distinguish them by their indices. */
                {"input", required_argument, 0, 'i'},
                {"output", required_argument, 0, 'o'},
                {0, 0, 0, 0}
            };
        /* getopt_long stores the option index here. */
        int option_index = 0;
        c = getopt_long (argc, argv, "i:o:",
                         long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        printf("%d\n", c);
        switch (c) {
            case 0:
              /* If this option set a flag, do nothing else now. */
              if (long_options[option_index].flag != 0)
                break;
              printf ("option %s", long_options[option_index].name);
              if (optarg)
                printf (" with arg %s", optarg);
              printf ("\n");
              break;
            case 'i':
                printf ("option -o with value `%s'\n", optarg);
                original_file = optarg;
                break;

            case 'o':
                printf ("option -i with value `%s'\n", optarg);
                output_file = optarg;
                break;
            case '?':
            /* getopt_long already printed an error message. */
                break;
            default:
                abort();
        }
    }
    // char* original_file = "../../../data/Data_MAT/jasperRidge2_F224_2.mat";
    // char* output_file = "../../../data/Data_MAT/abundances_c.mat";
    if (output_file == NULL || original_file == NULL) {
        puts("program need 2 arguments: input=<path_to_file> and output=<path_to_file>");
        return 1;
    }
    double start_time;
    double run_time;

    mxArray *orig_pa = NULL;
    double *orig_matrix;
    double *endmembers;
    double *pca_output;
    double *output;
    double *abundances;
    int orig_cols;
    int output_rows;
    int orig_rows;
    // TODO unsupervised!
    int q = 8;
    int iter = 2500;
    double *orig_data;

    // TODO use args
    orig_pa = load_variable_from_file(original_file, "Y");

    if (orig_pa == NULL) {
        printf("moje chyba v orig_pa\n");
        return 1;
    }


    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_cols = mxGetN(orig_pa);
    orig_matrix = malloc (orig_rows * orig_cols * sizeof(double));
    output = malloc(sizeof(double) * q * orig_cols);
    abundances = malloc(sizeof(double) * q * orig_cols);

    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_cols; j++) {
            orig_matrix[i*orig_cols + j] = orig_data[j*orig_rows + i];
        }
    }

    //start_time = wtime();
    //pca(orig_matrix, orig_rows, orig_cols, &pca_output, &output_rows);
    //run_time = wtime() - start_time;
    //puts("********************");
    //puts("******** PCA *******");
    //puts("********************");
    //printf("%f s\n", run_time);
    output_rows = orig_rows;
    endmembers = malloc(sizeof(double) * output_rows * q);

    start_time = wtime();
    ppi(endmembers, orig_matrix, output_rows, orig_cols, q, iter);
    run_time = wtime() - start_time;
    puts("********************");
    puts("******** PPI *******");
    puts("********************");
    printf("%f s\n", run_time);

    save_matrix_to_file(endmembers, orig_rows, q, "./endmembers-old.mat", "EM");

    // non constrained abundances
    start_time = wtime();
    int res = non_constrained_simple(abundances, orig_matrix, output_rows, orig_cols, endmembers, output_rows, q);
    run_time = wtime() - start_time;
    puts("********************");
    puts("******** NCS *******");
    puts("********************");
    printf("%f s\n", run_time);
    checkError(res, "Non constrained_simple");

    // isra
    start_time = wtime();
    res = isra(abundances, orig_matrix, output_rows, orig_cols, endmembers, output_rows, q);
    run_time = wtime() - start_time;
    puts("********************");
    puts("******** ISRA ******");
    puts("********************");
    printf("%f s\n", run_time);
    checkError(res, "isra");

    //sum to one
    start_time = wtime();
    res = sum_to_one(abundances, orig_cols, q);
    run_time = wtime() - start_time;
    puts("********************");
    puts("******** STO *******");
    puts("********************");
    printf("%f s\n", run_time);
    checkError(res, "sum_to_one");

    save_matrix_to_file(abundances, q, orig_cols, output_file, "abun_c");

    free(orig_matrix);
    free(output);
    free(abundances);
    free(endmembers);
    free(pca_output);
    float whole_time = wtime() - whole_start_time;
    puts("********************");
    puts("***** RUN TIME *****");
    puts("********************");

    printf("%f s\n", whole_time);

}

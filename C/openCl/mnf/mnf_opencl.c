#include <mat.h>
#include <math.h>
#include <mex.h>
#include <matlab_io.h>
#include <string.h>
#include <clBLAS.h>
#include <time.h>
#include <lapacke.h>

#include "err_code.h"

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif


void print_eigenvalues( char* desc, int n, double* wr, double* wi ) {
        int j;
        printf( "\n %s\n", desc );
   for( j = 0; j < n; j++ ) {
      if( wi[j] == (double)0.0 ) {
         printf( " %6.2f", wr[j] );
      } else {
         printf( " (%6.2f,%6.2f)", wr[j], wi[j] );
      }
         puts("----");
   }
   printf( "\n" );
}

/* Auxiliary routine: printing eigenvectors */
void print_eigenvectors( char* desc, int n, double* wi, double* v, int ldv ) {
        int i, j;
        printf( "\n %s\n", desc );
   for( i = 0; i < n; i++ ) {
      j = 0;
      while( j < n ) {
         if( wi[j] == (double)0.0 ) {
            printf( " %6.2f", v[i*ldv+j] );
            j++;
         } else {
            printf( " (%6.2f,%6.2f)", v[i*ldv+j], v[i*ldv+(j+1)] );
            printf( " (%6.2f,%6.2f)", v[i*ldv+j], -v[i*ldv+(j+1)] );
            j += 2;
         }
         puts("----");
      }
      printf( "\n" );
   }
}

void eigen(double *orig_matrix, int orig_rows, double *eigen_values, double *eigen_vectors) {
    double *copy_orig_matrix = malloc(sizeof(double) * orig_rows * orig_rows);
    memcpy(copy_orig_matrix, orig_matrix, sizeof(double) * orig_rows * orig_rows);
    int n = orig_rows;
    int lda = orig_rows;
    int info;
    int lwork = -1;
    double wkopt;
    double* work;
    /* Local arrays */
    int N = orig_rows;
    int LDVL = N;
    int ldvl = N;
    int ldvr = N;
    int LDVR = N;
    int LDA = N;
    double wi[N], vl[LDVL*N];
    /* Solve eigenproblem */
    info = LAPACKE_dgeev( LAPACK_ROW_MAJOR, 'V', 'V', n, copy_orig_matrix, lda, eigen_values, wi,
                    vl, ldvl, eigen_vectors, ldvr );
    // dsyev( "Vectors", "Upper", &n, a, &lda, w, &wkopt, &lwork, &info );
    if( info > 0 ) {
        printf( "The algorithm failed to compute eigenvalues.\n" );
        exit( 1 );
    }
    /* Print eigenvalues */
//    print_eigenvalues( "Eigenvalues", n, eigen_values, wi );
//    char* output_file = "../../../data/Data_MAT/C_scatter.mat";
//    save_matrix_to_file(eigen_values, 1, orig_rows, output_file, "e_values");
//    /* Print eigenvectors */
//    print_eigenvectors( "Eigenvectors (stored columnwise)", n, eigen_values, vl, ldvl );
//    /* Print eigenvectors */
//    print_eigenvectors( "Eigenvectors (stored columnwise)", n, eigen_values, eigen_vectors, ldvr );
}

//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

//int householder(float *a, int n, float d[], float e[]) {
//{
//    int l,k,j,i;
//    float scale,hh,h,g,f;
//    for (i=n-1;i>=1;i--) {
//        l=i-1;
//        h=scale=0.0;
//        if (l > 0) {
//            for (k=0;k<=l-1;k++)
//                scale += fabs(a[i * n + k]);
//            if (scale == 0.0)
//                e[i]=a[i][l];
//            else {
//                for (k=0;k<=l-1;k++) {
//                    a[i * n + k] /= scale;
//                     // Use scaled a’s for transformation.
//                    h += a[i * n + k]*a[i * n + k];
//                     // Form σ in h.
//                }
//                f=a[i * n + l];
//                g=(f >= 0.0 ? -sqrt(h) : sqrt(h));
//                e[i]=scale*g;
//                h -= f*g;
//                // Now h is equation (11.2.4).
//                a[i * n + l]=f-g;
//                // Store u in the ith row of a.
//                f=0.0;
//                for (j = 0; j <= l-1; j++) {
//                    /* Next statement can be omitted if eigenvectors not wanted */
//                    a[j][i]=a[i][j]/h;
//                    // Store u/H in ith column of a.
//                    g=0.0;
//                    // Form an element of A · u in g.
//                    for (k=1;k<=j;k++)
//                        g += a[j][k]*a[i][k];
//                    for (k=j+1;k<=l;k++)
//                        g += a[k][j]*a[i][k];
//                    e[j]=g/h;
//                    // Form element of p in temporarily unused element of e.
//                    f += e[j]*a[i][j];
//                }
//                hh=f/(h+h);
//                // Form K, equation (11.2.11).
//                for (j=1;j<=l;j++) {
//                    // Form q and store in e overwriting p.
//                    f=a[i][j];
//                    e[j]=g=e[j]-hh*f;
//                    for (k=1;k<=j;k++)
//                        // Reduce a, equation (11.2.13).
//                        a[j][k] -= (f*e[k]+g*a[i][k]);
//                }
//            }
//        } else
//            e[i]=a[i][l];
//        d[i]=h;
//    }
//    /* Next statement can be omitted if eigenvectors not wanted */
//    d[1]=0.0;
//    e[1]=0.0;
//    /* Contents of this loop can be omitted if eigenvectors not
//    wanted except for statement d[i]=a[i][i]; */
//    for (i=1;i<=n;i++) {
//        // Begin accumulation of transformation matrices
//        l=i-1;
//        if (d[i]) {
//            // This block skipped when i=1.
//            for (j=1;j<=l;j++) {
//                g=0.0;
//                for (k=1;k<=l;k++)
//                    // Use u and u/H stored in a to form P·Q.
//                    g += a[i][k]*a[k][j];
//                for (k=1;k<=l;k++)
//                    a[k][j] -= g*a[k][i];
//            }
//        }
//        d[i]=a[i][i];
//        // This statement remains.
//        a[i][i]=1.0;
//        // Reset row and column of a to identity matrix for next iteration.
//        for (j=1;j<=l;j++) a[j][i]=a[i][j]=0.0;
//    }
//}
//
int pca(double *image,int orig_rows,int orig_cols, double **output, int *output_rows) {
    //Compute the d-dimensional mean vector
    double *image_mean = malloc(sizeof(double) * orig_rows);

    for (int i = 0; i<orig_rows; i++) {
        image_mean[i] = 0.0;
        for (int j = 0; j<orig_cols; j++) {
            image_mean[i] += image[i*orig_cols + j];
        }
        image_mean[i] = image_mean[i] / orig_cols;
    }

    // Computing the Scatter Matrix
    double start_time;
    double run_time;
    start_time = wtime();

    // clBLAS init
    clblasOrder order = clblasRowMajor;

    // init OPENCL
    cl_device_id     device_id;     // compute device id
    cl_context       ctx;       // compute context
    cl_command_queue queue;      // compute command queue
    cl_program       program;       // compute program
    cl_kernel        kernel;
        
    cl_mem bufA, bufX, bufY, bufD;
    cl_int err;
    cl_event event = NULL;
    int ret = 0;

    // Fill vectors a and b with random float values

    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkError(err, "Finding platforms");
    if (numPlatforms == 0)
    {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++)
    {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS)
        {
            break;
        }
    }

    if (device_id == NULL)
        checkError(err, "Finding a device");

    err = output_device_info(device_id);
    checkError(err, "Printing device output");

    // Create a compute context
    ctx = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    checkError(err, "Creating context");

    // Create a command queue
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");
    run_time = wtime() - start_time;
    puts("INIT POINTERS and INIT OPENCL");
    printf("%f s\n", run_time);

    /**************************************
    * clBLAS
    *************************************/
    // clBLAS Setup
    err = clblasSetup();
    if (err != CL_SUCCESS) {
        printf("clblasSetup() failed with %d\n", err);
        clReleaseCommandQueue(queue);
        clReleaseContext(ctx);
        return 1;
    }


    start_time = wtime();
    double *vec1 = malloc(sizeof(double) * orig_rows);
    double *vec2 = malloc(sizeof(double) * orig_rows); 
    double *temp = malloc(sizeof(double) * orig_rows * orig_rows);
    double *scatter_matrix = malloc(sizeof(double) * orig_rows * orig_rows); 
    puts("AAAAAAAAAA");
    for (int i=0; i<orig_rows*orig_rows; i++) {
        scatter_matrix[i] = 0.0;
    }

    for (int i=0; i<orig_cols; i++) {
        for (int j=0; j<orig_rows; j++) {
            vec1[j] = image[j*orig_cols + i] - image_mean[j];
            vec2[j] = image[j*orig_cols + i] - image_mean[j];
        }
        queue = clCreateCommandQueue(ctx, device_id, 0, &err);
        checkError(err, "Creating command queue");
        bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * sizeof(double),
                              NULL, &err);
        bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * sizeof(double),
                              NULL, &err);
        bufY = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_rows * orig_rows * sizeof(double),
                              NULL, &err);

        
        err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
            orig_rows * sizeof(double), vec1, 0, NULL, NULL);

        err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
            orig_rows * sizeof(double), vec2, 0, NULL, NULL);
        err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
            orig_rows * orig_rows * sizeof(double), temp, 0, NULL, NULL);
        err = clblasDgemm(order, clblasNoTrans, clblasTrans, orig_rows, orig_rows, 1,
                          1.0, bufA, 0, 1,
                          bufX, 0, 1,
                          0.0, bufY, 0, orig_rows, 1, &queue, 0, NULL, &event);

        if (err != CL_SUCCESS) {
            printf("clblasDgemv() failed with %d\n", err);
            ret = 1;
            return ret;
        }
        else {
            /* Wait for calculations to be finished. */
            err = clWaitForEvents(1, &event);
            /* Fetch results of calculations from GPU memory. */
            err = clEnqueueReadBuffer(queue, bufY, CL_TRUE, 0, orig_rows * orig_rows * sizeof(double),
                                      temp, 0, NULL, NULL);
            checkError(err, "returning result");
            for (int j=0; j < orig_rows*orig_rows; j++){
                scatter_matrix[j] += temp[j];
            }

        }
        clReleaseMemObject(bufA);
        clReleaseMemObject(bufX);
        clReleaseMemObject(bufY);

        clReleaseCommandQueue(queue);
    }
    char* output_file = "../../../data/Data_MAT/C_scatter.mat";
    save_matrix_to_file(scatter_matrix, orig_rows, orig_rows, output_file, "scatter_c");
    
    free(vec1);
    free(vec2);
    free(temp);
    run_time = wtime() -  start_time;
    printf("SCATTER MATRIX...\n");
    printf("%f s\n", run_time);
    /***************************************************
    * EigenValues
    ***************************************************/
    double *eigen_values = malloc(sizeof(double) * orig_rows);
    double *eigen_vectors = malloc(sizeof(double) * orig_rows * orig_rows);

    eigen(scatter_matrix, orig_rows, eigen_values, eigen_vectors);

    int bottom_index = orig_rows;
    for (int i=0; i<orig_rows; i++ ) {
        if (eigen_values[i] < 1.0) {
            bottom_index = i;
            break;
        }
    }
    // take vectors
    double *final_eigen_vectors = malloc(sizeof(double) * orig_rows * bottom_index);
    for (int i=0; i<bottom_index; i++) {
        for (int j=0; j<orig_rows; j++) {
            final_eigen_vectors[j*bottom_index + i] = eigen_vectors[j*orig_rows + i];
        }
    }

    double *temp_output = malloc(sizeof(double) * orig_cols * bottom_index);
    *output_rows = bottom_index;
    for (int i=0; i<10; i++) {
        printf("%f\n", temp_output[i]);
    }

    // promitnuti
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");
    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * bottom_index * sizeof(double),
                          NULL, &err);
    bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(double),
                          NULL, &err);
    bufY = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_cols * bottom_index * sizeof(double),
                          NULL, &err);

    
    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
        orig_rows * bottom_index * sizeof(double), final_eigen_vectors, 0, NULL, NULL);

    err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
        orig_rows * orig_cols * sizeof(double), image, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
        bottom_index * orig_cols * sizeof(double), temp_output, 0, NULL, NULL);
    err = clblasDgemm(order, clblasTrans, clblasNoTrans, bottom_index, orig_cols, orig_rows,
                      1.0, bufA, 0, bottom_index,
                      bufX, 0, orig_cols,
                      0.0, bufY, 0, orig_cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufY, CL_TRUE, 0, orig_cols * bottom_index * sizeof(double),
                                  temp_output, 0, NULL, NULL);
        checkError(err, "returning result");
    }
    *output = temp_output;
    clReleaseMemObject(bufA);
    clReleaseMemObject(bufX);
    clReleaseMemObject(bufY);

    clReleaseCommandQueue(queue);
    return 0; 
}

int main() {
    char* original_file = "../../../data/Data_MAT/jasperRidge2_F224_2.mat";
    char* output_file = "../../../data/Data_MAT/C_pca.mat";

    mxArray *orig_pa = NULL;
    double *orig_matrix;
    double *output;
    int orig_cols;
    int output_rows;
    int orig_rows;
    int q = 3;
    int iter = 1000;
    double *orig_data;

    orig_pa = load_variable_from_file(original_file, "Y");

    if (orig_pa == NULL) {
        printf("moje chyba v orig_pa\n");
        return 1;
    }


    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_cols = mxGetN(orig_pa);
    orig_matrix= malloc (orig_rows * orig_cols * sizeof(double));

    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_cols; j++) {
            orig_matrix[i*orig_cols + j] = orig_data[j*orig_rows + i];
        }
    }



    /***************************************************
     * PPI ALGORITHM
     ***************************************************/
    double run_time;
    double start_time = wtime();

    pca(orig_matrix, orig_rows, orig_cols, &output, &output_rows);
    run_time = wtime() - start_time;
    for (int i=0; i<10; i++) {
        printf("%d\n", output_rows);
    }

    printf("PPI WHOLE ALGORITHM: %f s\n", run_time);


    save_matrix_to_file(output, output_rows, orig_cols, output_file, "pca_c");

    free(orig_matrix);
    //free(output);
    return 0;

    return 0;

}

#include <fcls.h>
#include <ppi.h>
#include <transpose.h>
#include <mex.h>
//#include <pca.h>
#include <matlab_io.h>
#include <getopt.h>
#include <calibration_matrix.h>

/* Flag set by ‘--verbose’. */


#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS


static int verbose_flag;
extern int output_device_info(cl_device_id );
extern double wtime();

extern int pca(cl_float *image,int orig_rows,int orig_cols, cl_float **output, int *output_rows);
extern mxArray* load_variable_from_file(char *file_name, char *variable_name);
extern void save_matrix_to_file(cl_float *matrix, int rows, int cols, char *file_name, char *variable_name);
extern int ppi(cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int q, cl_mem d_skewers, int iter, cl_context ctx, cl_device_id device_id);
extern int generate_random_skewers(cl_mem skewers, int rows, int cols, cl_context ctx, cl_device_id device_id);
extern int create_identity_matrix(cl_float *matrix, int cols, int rows);
extern int qr_algorithm(cl_float *matrix, int cols, int rows, cl_float *eigen_values, cl_float *eigen_vectors);
extern int transpose(cl_mem d_matrix, cl_mem d_out_matrix, int rows, int cols, cl_context ctx, cl_device_id device_id);
extern int normalize_matrix(cl_mem d_matrix, int rows, int cols, int by_band, cl_context ctx, cl_device_id device_id);


int main(int argc, char **argv) {
    // TODO use args
    //
    double whole_start_time = wtime();
    int c;
    int err;
    char* output_file = NULL;
    char* original_file = NULL;
    char* variable = NULL;
    char* output_variable = "ABUNnew";
    static int normalize_by_band = 0;
    static int need_normalize = 0;
    static int use_filter_long = 0;
    static int use_filter_short = 0;
    int num_bands = 0;
    float *filter_low = malloc(sizeof(cl_float) * 25 *25);
    float *filter_long = malloc(sizeof(cl_float) * 25 *25);
    while (1) {
        static struct option long_options[] =
            {
                /* These options set a flag. */
                {"verbose", no_argument, &verbose_flag, 1},
                {"brief", no_argument, &verbose_flag, 0},
                /* These options don’t set a flag.
                   We distinguish them by their indices. */
                {"input", required_argument, 0, 'i'},
                {"variable", required_argument, 0, 'v'},
                {"output", required_argument, 0, 'o'},
                {"output_variable", optional_argument, 0, 's'},
                {"normalize", no_argument, &need_normalize, 1},
                {"normalize_band", no_argument, &normalize_by_band, 1},
                {"filter_long", no_argument, &use_filter_long, 1},
                {"filter_short", no_argument, &use_filter_short, 1},
                {"bands", optional_argument, 0, 'b'},
                {0, 0, 0, 0}
            };
        /* getopt_long stores the option index here. */
        int option_index = 0;
        c = getopt_long (argc, argv, "i:o:",
                         long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        printf("%d\n", c);
        switch (c) {
            case 0:
              /* If this option set a flag, do nothing else now. */
              if (long_options[option_index].flag != 0)
                break;
              printf ("option %s", long_options[option_index].name);
              if (optarg)
                printf (" with arg %s", optarg);
              printf ("\n");
              break;
            case 'i':
                printf ("option -o with value `%s'\n", optarg);
                original_file = optarg;
                break;

            case 'v':
                printf ("option -v with value `%s'\n", optarg);
                variable = optarg;
                break;

            case 'o':
                printf ("option -i with value `%s'\n", optarg);
                output_file = optarg;
                break;
            case 's':
                printf ("option -s with value `%s'\n", optarg);
                output_variable = optarg;
                break;
            case 'b':
                printf ("option -b with value `%s'\n", optarg);
                num_bands = atoi(optarg);
                break;
            case '?':
            /* getopt_long already printed an error message. */
                break;
            default:
                abort();
        }
    }
    if (output_file == NULL || original_file == NULL) {
        puts("program need 2 arguments: input=<path_to_file> and output=<path_to_file>");
        return 1;
    }

    /**********************************
    * INIT OPENCL
    **********************************/
    cl_platform_id platform = 0;
    cl_device_id device_id = NULL;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
    cl_context ctx = 0;

    cl_mem d_image;
    cl_mem d_em_em;
    cl_mem d_filter;
    cl_mem d_corrected_image;
    cl_mem d_corrected_image_t;
    cl_mem d_skewers;

    /* Setup OpenCL environment. */
    cl_uint numPlatforms;
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkError(err, "Finding platforms");
    if (numPlatforms == 0)
    {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    checkError(err, "Getting platforms");
    for (int i = 0; i < numPlatforms; i++)
    {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS)
        {
            break;
        }
    }

    if (device_id == NULL) {
        checkError(err, "Finding a device");
    }

    ctx = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    checkError(err, "Creating context");
    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating queue");
    err = clblasSetup();
    checkError(err, "clblasSetup() failed");

    //time variables
    double start_time;
    double run_time;
    double steps;
    double test_time;

    //data variables
    mxArray *orig_pa = NULL;
    cl_float *orig_matrix;
    cl_float *endmembers;
    cl_float *pca_output;
    cl_float *output;
    cl_float *abundances;
    cl_float *skewers;
    int low_filter_bands = 24;
    int low_filter_coeff = 25;
    int long_filter_bands = 25;
    int long_filter_coeff = 25;
    int filter_bands = 0;
    int filter_coeff = 0;
    int orig_cols;
    int output_rows;
    int orig_rows;
    // TODO unsupervised!
    int q = 3;
    if (num_bands > 0) {
        q = num_bands;
    }
    int iter = q*64;
    double *orig_data;


    // Loading image data
    orig_pa = load_variable_from_file(original_file, variable);
    run_time = wtime() - test_time;
    printf("LOADING MATRIX %f\n", run_time);

    if (orig_pa == NULL) {
        printf("Chyba v nacteni dat\n");
        return 1;
    }


    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_cols = mxGetN(orig_pa);
    orig_matrix = malloc (orig_rows * orig_cols * sizeof(cl_float));

    output = malloc(sizeof(cl_float) * q * orig_cols);
    abundances = malloc(sizeof(cl_float) * q * orig_cols);

    test_time = wtime();
    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_cols; j++) {
            orig_matrix[i*orig_cols + j] = (cl_float)orig_data[j*orig_rows + i];
        }
    }
    //filter_bands = orig_rows;



    test_time = wtime();
    /*******************************************
    ********* CREATING BUFFER IMAGE  ***********
    ********************************************/
    clblasOrder order = clblasRowMajor;
    cl_event event = NULL;

    // NOT USING FILTER
    if (use_filter_long == 0 && use_filter_short == 0) {
        filter_bands = orig_rows;
        d_corrected_image_t = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
            filter_bands * orig_cols * sizeof(cl_float), orig_matrix, &err);
        checkError(err, "Copying original image");
        if (need_normalize == 1) {
            start_time = wtime();
            normalize_matrix(d_corrected_image_t, filter_bands,  orig_cols, (int)normalize_by_band, ctx, device_id);
            run_time = wtime() - start_time;
            printf("NORMALIZED IN: %f\n", run_time);
        }
    } else if (use_filter_long == 1  || use_filter_short == 1) {
        // USING FILTER
            create_calibration_matrix("../calibration_matrix/calibration_data.xml",  filter_low, filter_long);
        if (use_filter_long == 1) {
            filter_bands = long_filter_bands;
            d_filter = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                low_filter_coeff * orig_rows * sizeof(cl_float), filter_long, &err);
            checkError(err, "Creating and filling buffer d_image");

        } else {
            filter_bands = low_filter_bands;
            d_filter = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                low_filter_coeff * orig_rows * sizeof(cl_float), filter_low, &err);
            checkError(err, "Creating and filling buffer d_image");

        }


        d_corrected_image = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
            filter_bands * orig_cols * sizeof(cl_float), NULL, &err);
        checkError(err, "creating corrected image buffer");

        d_corrected_image_t = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
            filter_bands * orig_cols * sizeof(cl_float), NULL, &err);
        checkError(err, "creating corrected image buffer");




        d_image = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
            orig_rows * orig_cols * sizeof(cl_float), orig_matrix, &err);
        checkError(err, "Copying original image");

        start_time = wtime();

        err = clblasSgemm(order, clblasTrans, clblasTrans, orig_cols, filter_bands, orig_rows,
                          1.0, d_image, 0, orig_cols,
                          d_filter, 0, orig_rows,
                          0.0, d_corrected_image, 0, filter_bands, 1, &queue, 0, NULL, &event);
        if (err != CL_SUCCESS) {
            printf("clblasDgemv() failed with %d\n", err);
            return 1;
        }
        else {
            err = clWaitForEvents(1, &event);
        }
        printf("CORRECTION OF FILTER IN %f\n", wtime() - start_time);





        double pche = wtime();
        transpose(d_corrected_image, d_corrected_image_t, orig_cols, filter_bands,  ctx, device_id);
        printf("TRANSPOSING MATRIX IN %f\n", wtime() - pche);

        if (need_normalize == 1 || normalize_by_band == 1) {
            start_time = wtime();
            normalize_matrix(d_corrected_image_t, filter_bands, orig_cols, (int)normalize_by_band, ctx, device_id);
            printf("NORMALIZE IN %f\n", wtime() - start_time);
        }

        //cl_float *corrected = malloc(orig_cols * filter_bands * sizeof(cl_float));
        //err = clEnqueueReadBuffer(queue, d_corrected_image, CL_TRUE, 0,
        //    orig_cols * filter_bands * sizeof(cl_float), corrected_t, 0, NULL, NULL);
        //checkError(err, "Reading not normalized image");
        //clFinish(queue);
        //save_matrix_to_file(corrected, orig_cols, filter_bands, "t_not_normalized.mat", "orig_nn");

        //cl_float *corrected_t = malloc(orig_cols * filter_bands * sizeof(cl_float));
        //err = clEnqueueReadBuffer(queue, d_corrected_image_t, CL_TRUE, 0,
        //    orig_cols * filter_bands * sizeof(cl_float), corrected_t, 0, NULL, NULL);
        //checkError(err, "Reading normalized image");
        //save_matrix_to_file(corrected_t, filter_bands, orig_cols, "t_normalized.mat", "orig_n");
        clReleaseMemObject(d_image);
        clReleaseMemObject(d_corrected_image);
    }
    //cl_float *corrected_t = malloc(orig_cols * filter_bands * sizeof(cl_float));
    //err = clEnqueueReadBuffer(queue, d_corrected_image_t, CL_TRUE, 0,
    //    orig_cols * filter_bands * sizeof(cl_float), corrected_t, 0, NULL, NULL);
    //checkError(err, "Reading normalized image");
    //save_matrix_to_file(corrected_t, filter_bands, orig_cols, "t_normalized.mat", "orig_n");


    /*********************************************
    ********* CREATING SKEWERS FOR PPI ***********
    **********************************************/
    endmembers = malloc(sizeof(cl_float) * q * filter_bands);

    d_skewers = clCreateBuffer(ctx, CL_MEM_READ_ONLY, filter_bands * iter * sizeof(cl_float),
                          NULL, &err);
    checkError(err, "Creating buffer d_skewers");
    checkError(err, "Filling d_skewers");
    output_rows = filter_bands;
    int res;

    //start_time = wtime();
    //res = pca(orig_matrix, filter_bands, orig_cols, &pca_output, &output_rows);
    //checkError(res, "Principal component analysis");
    //run_time = wtime() - start_time;
    //puts("********************");
    //puts("******** PCA *******");
    //puts("********************");
    //printf("%f s\n", run_time);
    //endmembers = malloc(sizeof(cl_float) * output_rows * q);


    cl_mem d_endmembers = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
        q * filter_bands * sizeof(cl_float), NULL, &err);
    cl_mem d_abundances = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
        q * orig_cols * sizeof(cl_float), NULL, &err);
    checkError(err, "Creating buffer d_abundaces");

    d_em_em = clCreateBuffer(ctx, CL_MEM_READ_WRITE, q * filter_bands * sizeof(cl_float),
                          NULL, &err);
    checkError(err, "Creating buffer d_em_em");


    //err = clEnqueueWriteBuffer(queue, d_endmembers, CL_TRUE, 0,
    //    filter_bands * q * sizeof(int), endmembers, 0, NULL, NULL);
    //checkError(err, "Writing into d_endmembers");
    err = clEnqueueWriteBuffer(queue, d_abundances, CL_TRUE, 0,
        orig_cols * q * sizeof(int), abundances, 0, NULL, NULL);
    checkError(err, "Writing into d_abundances");
    clFinish(queue);

    run_time = wtime() - test_time;
    printf("END OF MANAGING MEMORY %f\n", run_time);


    start_time = wtime();
    steps = wtime();

    generate_random_skewers(d_skewers, filter_bands, iter, ctx, device_id);
    run_time = wtime() - start_time;
    puts("GENERATING RANDOM SKEWERS:");
    printf("%f s\n", run_time);

    /**********************************************
     ********* UNMIXING CHAIN *********************
     *********************************************/
    int count_iter = 1;
    double isra_time = 0;
    double ucls_time = 0;
    double sto_time = 0;
    double ppi_time = 0;
    for (int i=0; i<count_iter; i++) {
    start_time = wtime();
    res = ppi(d_corrected_image_t, filter_bands, orig_cols,
        d_endmembers, q, d_skewers, iter,
        ctx, device_id);
    run_time = wtime() - start_time;
    //puts("********************");
    //puts("******** PPI *******");
    //puts("********************");
    //printf("%f s\n", run_time);
    ppi_time += run_time;
    checkError(res, "PPI");

    //err = clEnqueueReadBuffer(queue, d_endmembers, CL_TRUE, 0,
    //    filter_bands * q * sizeof(cl_float), endmembers, 0, NULL, NULL);
    //checkError(err, "Reading abundances");
    //clFinish(queue);
    //save_matrix_to_file(endmembers, filter_bands, q, "./out/fr/endt.mat", "ent");

    start_time = wtime();
    res = non_constrained(
        d_abundances, d_corrected_image_t, filter_bands, orig_cols,
        d_endmembers, filter_bands, q, d_em_em,
        ctx, device_id);
    run_time = wtime() - start_time;
    //puts("******************************");
    //puts("******** NCLS - SIMPLE *******");
    //puts("******************************");
    //printf("%f s\n", run_time);
    ucls_time += run_time;
    checkError(res, "Non constrained");


    //err = clEnqueueReadBuffer(queue, d_abundances, CL_TRUE, 0,
    //    orig_cols * q * sizeof(cl_float), abundances, 0, NULL, NULL);
    //checkError(err, "Reading abundances");
    //clFinish(queue);
    //save_matrix_to_file(abundances, q, orig_cols, "./out/noisrat.mat", "noisrat");


    // isra
    start_time = wtime();
    res = isra(d_abundances,
        d_corrected_image_t, filter_bands, orig_cols,
        d_endmembers, filter_bands, q, d_em_em,
        ctx, device_id);
    run_time = wtime() - start_time;
    //puts("********************");
    //puts("******** ISRA ******");
    //puts("********************");
    //printf("%f s\n", run_time);
    isra_time += run_time;
    checkError(res, "isra");

    char isra_out [10];
    sprintf(isra_out, "isra_%d", i);
    err = clEnqueueReadBuffer(queue, d_abundances, CL_TRUE, 0,
        orig_cols * q * sizeof(cl_float), abundances, 0, NULL, NULL);
    checkError(err, "Reading abundances");
    clFinish(queue);
    save_matrix_to_file(abundances, q, orig_cols, "./out/israt.mat", isra_out);



    //sum to one
    start_time = wtime();
    res = sum_to_one(
        d_abundances, q, orig_cols,
        ctx, device_id);
    run_time = wtime() - start_time;
    //puts("********************");
    //puts("******** STO *******");
    //puts("********************");
    //printf("%f s\n", run_time);
    sto_time += run_time;
    checkError(res, "sum_to_one");
    }


    run_time = wtime() - steps;
    puts("******************");
    puts("***** STEPS ******");
    puts("******************");
    printf("%f s\n", run_time/count_iter);
    printf("ppi %f s\n", ppi_time/count_iter);
    printf("ucls %f s\n", ucls_time/count_iter);
    printf("isra %f s\n", isra_time/count_iter);
    printf("sto %f s\n", sto_time/count_iter);






    err = clEnqueueReadBuffer(queue, d_abundances, CL_TRUE, 0,
        orig_cols * q * sizeof(cl_float), abundances, 0, NULL, NULL);
    checkError(err, "Reading abundances");
    clFinish(queue);
    save_matrix_to_file(abundances, q, orig_cols, output_file, output_variable);

    free(orig_matrix);
    free(output);
    free(abundances);
    free(endmembers);
    free(pca_output);
    free(filter_low);
    free(filter_long);
    clReleaseMemObject(d_em_em);
    clReleaseMemObject(d_corrected_image_t);
    clReleaseMemObject(d_skewers);
    clReleaseMemObject(d_endmembers);
    clReleaseMemObject(d_abundances);
    clReleaseContext(ctx);
    clReleaseCommandQueue(queue);
    float whole_time = wtime() - whole_start_time;
    puts("********************");
    puts("***** RUN TIME *****");
    puts("********************");
    printf("%f s\n", whole_time);
}

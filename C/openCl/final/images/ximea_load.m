function [ image ] = ximea_load( input_name )
    n = 215;
    m = 409;
    p = 25;
    raw = multibandread(input_name, [n, m, p], 'uint16', 0, 'bsq', 'ieee-le');
    raw_t = permute(conj(raw), [2,1,3]);
    image = reshape(raw_t, m*n, p)';
end
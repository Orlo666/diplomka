#include <mat.h>
#include <math.h>
#include <mex.h>
#include <gsl/gsl_sort.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_sort.h"
#include "gsl/gsl_sort_vector.h"
#include "../matlab_io.h"
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>
#include <sys/time.h>

int main() {
    char* end_members_file = "../../data/Data_MAT/end3.mat";
    char* original_file = "../../data/Data_MAT/samson_1.mat";
    char* covariance_file = "../../data/Data_MAT/covariance.mat";
    char* output_file = "../../data/Data_MAT/C_abunda.mat";

    mxArray *orig_pa = NULL;
    gsl_matrix *orig_matrix;
    gsl_matrix *orig_backup;
    gsl_matrix *end_members;
    gsl_matrix *end_members_bckp;
    gsl_matrix *output;
    int orig_columns, end_cols;
    int orig_rows, end_rows;
    int iter = 1000;
    double *orig_data;
    double *end_data;

    orig_pa = load_variable_from_file(original_file, "V");

    if (orig_pa == NULL) {
        printf("moje chyba v orig_pa\n");
        return 1;
    }

    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_columns = mxGetN(orig_pa);
    orig_matrix= gsl_matrix_alloc (orig_rows, orig_columns);

    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_columns; j++) {
            gsl_matrix_set(orig_matrix, i, j, orig_data[j*orig_rows + i]);
        }
    }



    orig_pa = load_variable_from_file(end_members_file, "M");

    end_rows = mxGetM(orig_pa);
    end_cols = mxGetN(orig_pa);
 
    end_members = gsl_matrix_alloc(end_rows, end_cols);
    end_members_bckp = gsl_matrix_alloc(end_rows, end_cols);
    orig_data = mxGetPr(orig_pa);
    


    for (int i = 0; i < end_rows; i++) {
        for (int j = 0; j < end_cols; j++) {
            gsl_matrix_set(end_members, i, j, orig_data[j*end_rows + i]);
            gsl_matrix_set(end_members_bckp, i, j, orig_data[j*end_rows + i]);
        }
    }
    

    
    int count = end_cols;
    int done = 0;
    ///////////
    gsl_matrix * m = gsl_matrix_alloc (end_cols, end_cols);



    gsl_matrix *three_iter = gsl_matrix_alloc(end_cols, 1);
    double vector_result;
    double right_result;






    gsl_matrix *one_field_matrix = gsl_matrix_alloc(1, 1);
    gsl_matrix *one_field_inverse = gsl_matrix_alloc(1, 1);
    gsl_permutation * one_field_p = gsl_permutation_alloc (1);

    
    int *ref = malloc(end_cols * sizeof(int));
    double temp = 0.0;
    int max_idx = -1;
    double max_val = 0.0;
    gsl_matrix_view end_view;


    output = gsl_matrix_alloc(end_cols, orig_columns);
    //gsl_matrix *three_iter = gsl_matrix_alloc(end_cols, 1);
    gsl_matrix *MM;
    gsl_permutation * p;
    gsl_matrix * inverse;
    gsl_matrix *sec_iter;
    gsl_vector *als_hat;
    gsl_vector *afcls_hat;
    gsl_vector *negative_idx;
    gsl_vector *s_vector;
    gsl_vector *ones_vector;
    gsl_vector *temp_vector;

    
    bool end_while;
    gsl_vector *alpha = gsl_vector_alloc(end_cols);
    for (int n=0; n < orig_columns; n++) {
        done = 0;
        gsl_vector_view orig_vector_view = gsl_matrix_column(orig_matrix, n);
        

        gsl_matrix_memcpy (end_members, end_members_bckp);
        count = end_cols;



        for (int i=0; i<end_cols; i++) {
            ref[i] = i;
        }

        

        end_view = gsl_matrix_submatrix(end_members, 0, 0, end_rows, end_cols);
        while (done == 0) {
            end_while = true;

            
            int s = 0;           
            
            
            //init
            MM = gsl_matrix_alloc(count, count);
            p = gsl_permutation_alloc (count);
            inverse = gsl_matrix_alloc (count, count);
            sec_iter = gsl_matrix_alloc(count, end_rows);
            als_hat = gsl_vector_alloc(count);
            afcls_hat = gsl_vector_alloc(count);
            negative_idx = gsl_vector_alloc(count);
            s_vector = gsl_vector_alloc(count);
            ones_vector = gsl_vector_alloc(count);
            gsl_vector_set_all(ones_vector, 1.0);
            temp_vector = gsl_vector_alloc(count);

            gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1.0, &end_view.matrix, &end_view.matrix, 0.0, MM);

        


            gsl_linalg_LU_decomp (MM, p, &s);    
            gsl_linalg_LU_invert (MM, p, inverse);

            gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, inverse, &end_view.matrix, 0.0, sec_iter);
            gsl_blas_dgemv (CblasNoTrans, 1.0, sec_iter, &orig_vector_view.vector, 0.0, als_hat);


            //s = inv(U.'*U)*ones(count, 1);
            //

            gsl_blas_dgemv (CblasNoTrans, 1.0, inverse, ones_vector, 0.0, s_vector);
            // afcls_hat = als_hat - inv(U.'*U)*ones(count, 1)*inv(ones(1, count)*inv(U.'*U)*ones(count, 1))*(ones(1, count)*als_hat-1)
            // s_vector = inv(U.'*U)*ones(count, 1)
            //
            // inv(ones(1, count)*inv(U.'*U)*ones(count, 1))

            gsl_blas_ddot(ones_vector, s_vector, &vector_result);

            gsl_matrix_set(one_field_matrix, 0, 0, vector_result);



            s = 0;
            gsl_linalg_LU_decomp (one_field_matrix, one_field_p, &s);

            gsl_linalg_LU_invert (one_field_matrix, one_field_p, one_field_inverse);
            

            gsl_blas_ddot(ones_vector, als_hat, &right_result);
            right_result -= 1.0;
            
            gsl_vector_memcpy(temp_vector, s_vector);
            gsl_vector_scale (temp_vector, gsl_matrix_get(one_field_inverse, 0, 0));
            gsl_vector_scale (temp_vector, right_result);

            gsl_vector_memcpy(afcls_hat, als_hat);
            gsl_vector_sub(afcls_hat, temp_vector);


            //tiding up

            gsl_matrix_free(MM);
            gsl_matrix_free(inverse);
            gsl_matrix_free(sec_iter);

            gsl_permutation_free(p);

            gsl_vector_free(als_hat);
            gsl_vector_free(ones_vector);
            gsl_vector_free(temp_vector);



            for (int i=0; i<count; i++) {
                if (gsl_vector_get(afcls_hat, i) < 0.0) {
                    //TODO return fallback
                    end_while = false;
                }
            }


            if (end_while) {
                int temp_index = 0;
                for (int i=0; i<count; i++) {
                    gsl_vector_set(alpha, ref[i], gsl_vector_get(afcls_hat, i));
                }
                gsl_vector_free(afcls_hat);
                break;
            }


//
//        idx = find(afcls_hat<0);
//        afcls_hat(idx) = afcls_hat(idx) ./ s(idx);
//        [val, maxIdx] = max(abs(afcls_hat(idx)));
//        maxIdx = idx(maxIdx);
//        alpha(maxIdx) = 0;
//        keep = setdiff(1:size(U, 2), maxIdx);
//        U = U(:, keep);
//        count = count - 1;
//        ref = ref(keep);
//
            int idx_i = 0;
            max_idx = -1;
            max_val = -1.0;
            for (int i=0; i<count; i++) {
                if (gsl_vector_get(afcls_hat, i) < 0.0) {
                    gsl_vector_set(negative_idx, idx_i, i);
                    idx_i ++;

                    temp = gsl_vector_get(afcls_hat, i) / gsl_vector_get(s_vector, i);
                    gsl_vector_set(afcls_hat, i, temp);
                    if (max_val < fabs(temp)) {
                        max_val = fabs(temp);
                        max_idx = i;
                    }
                }
            }

            gsl_vector_set(alpha, max_idx, 0.0);

            // next tiding up
            gsl_vector_free(afcls_hat);
            gsl_vector_free(negative_idx);
            gsl_vector_free(s_vector);

            for (int i=0; i<count; i++) {
                if (i > max_idx) {

                    gsl_matrix_swap_columns(&end_view.matrix, i-1, i);

                }

            }
            int idx_j = 0;
            for(int i=0; i< end_cols; i++) {
                if (i != max_idx) {
                    ref[idx_j] = i;
                    idx_j++;
                }
            }


            end_view = gsl_matrix_submatrix(&end_view.matrix, 0, 0, end_rows, count-1);

            count --;
            
        }

        gsl_matrix_set_col(output, n, alpha);
        if (n<2) {
        for (int i=0; i<end_cols; i++) {
            printf("%f\n", gsl_vector_get(alpha, i));
        }
        for (int i=0; i<end_cols; i++) {
            printf("%f\n", gsl_matrix_get(output, i, n));
        }

        printf("--------------------------\n");
        }

    }
    gsl_vector_free(alpha);   

    ///////////

    save_matrix_to_file(output, output_file, "abun_c");
    
    gsl_matrix_free(orig_matrix);
    gsl_matrix_free(output);

}

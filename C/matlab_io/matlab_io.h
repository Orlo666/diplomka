#ifndef MATLABIO_H
#define MATLABIO_H
#ifndef __MULT_HDR
#define __MULT_HDR


#include "mat.h"
#include "mex.h"
#include <gsl/gsl_matrix.h>

mxArray* load_variable_from_file(char *file_name, char *variable_name);
void save_matrix_to_file_gsl(gsl_matrix *matrix, char *file_name, char *variable_name);
void save_matrix_to_file(float *matrix, int rows, int cols, char *file_name, char *variable_name);

#endif
#endif

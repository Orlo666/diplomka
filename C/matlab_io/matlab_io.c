#include <matlab_io.h>

mxArray* load_variable_from_file(char *file_name, char *variable_name) {
    MATFile *pmat_file;
    mxArray *pa;

    pmat_file = matOpen(file_name, "r");
    if (pmat_file == NULL) {
        printf("Error reopening file %s\n", file_name);
        exit(EXIT_FAILURE);
    }
    pa = matGetVariable(pmat_file, variable_name);
    if (pa == NULL) {
        printf("Cannot read %s\n", variable_name);
        exit(EXIT_FAILURE);
    }
    return pa;
}

void save_matrix_to_file_gsl(gsl_matrix *matrix, char *file_name, char *variable_name) {
    if (matrix == NULL) {
        printf("WRONG inputing data\n");
        return;
    }
    MATFile *output_file;
    output_file = matOpen(file_name, "w");
    if (output_file == NULL) {
        printf("Error reopening file %s\n", file_name);
        exit(EXIT_FAILURE);
    }
    double *dynamic_data = (double *)mxMalloc(matrix->size1*matrix->size2*sizeof(double));

    for (int m = 0; m < matrix->size1; m++) {
        for (int n = 0; n < matrix->size2; n++) {
            dynamic_data[n*matrix->size1 + m] = gsl_matrix_get(matrix, m, n);
        }
    }


    mxArray *output_array = mxCreateDoubleMatrix(matrix->size1, matrix->size2, mxREAL);
    mxSetPr(output_array, dynamic_data);

    matPutVariable(output_file, variable_name, output_array);
    mxFree(dynamic_data);
}

void save_matrix_to_file(float *matrix, int rows, int cols, char *file_name, char *variable_name) {
    if (matrix == NULL) {
        printf("WRONG inputing data\n");
        exit(EXIT_FAILURE);
    }
    MATFile *output_file;

    output_file = matOpen(file_name, "w");

    if (output_file == NULL) {
        printf("Error reopening file %s\n", file_name);
        exit(EXIT_FAILURE);
    }

    double *dynamic_data = (double *)mxMalloc(cols * rows * sizeof(double));

    for (int m = 0; m < rows; m++) {
        for (int n = 0; n < cols; n++) {
            dynamic_data[n*rows + m] = (double)matrix[m * cols + n];
        }
    }


    mxArray *output_array = mxCreateDoubleMatrix(rows, cols, mxREAL);
    mxSetPr(output_array, dynamic_data);
    matPutVariable(output_file, variable_name, output_array);
    mxFree(dynamic_data);
}

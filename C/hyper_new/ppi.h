#ifndef PPI_H
#define PPI_H

#ifndef __MULT_HDR
#define __MULT_HDR

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <string.h>
#include <stdlib.h>
#include <clBLAS.h>
#include <time.h>

#include <err_code.h>

/*
 *
 */

int ppi(cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int q, cl_mem d_skewers, int iter,
        cl_context ctx, cl_device_id device_id);

int generate_random_skewers(cl_mem skewers, int rows, int cols,
    cl_context ctx, cl_device_id device_id);

#endif
#endif

int d_rand (int seed) // 1<=seed<m
{
    int const a    = 16807;      //ie 7**5
    int const m    = 2147483647; //ie 2**31-1
    seed = ((long)(seed * a))%m;
    return seed;
}
__kernel void generate_skewers(
    __global double *skewers,
    const int cols
){
    double random_number = (double)d_rand(get_global_id(1) + 1) / (double)d_rand(get_global_id(0) + 1);
    skewers[get_global_id(0) * cols + get_global_id(1)] = random_number;
}



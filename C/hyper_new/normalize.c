#include <normalize.h>

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#define WAVEFRONT 1024

int normalize_matrix(cl_mem d_matrix, int rows, int cols, int by_band, cl_context ctx, cl_device_id device_id) {
    cl_int err;
    cl_event event = NULL;
    cl_program program;       // compute program
    int ret = 0;
    cl_kernel kernel;

    char *sum_to_one_kernel = getKernelSource("../../hyper_new/normalize.cl");
    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating queue");

    /* Setup OpenCL environment. */
    program = clCreateProgramWithSource(ctx, 1, (const char **) & sum_to_one_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    size_t global;
    size_t local;

    if (by_band == 1) {
        kernel = clCreateKernel(program, "normalize_matrix_by_band", &err);
        checkError(err, "Creating kernel");
        global = rows;
        local = rows;
    } else {
        kernel = clCreateKernel(program, "normalize_matrix", &err);
        checkError(err, "Creating kernel");
        global = cols;
        local = WAVEFRONT;
        int mod = cols % local;
        if (mod > 0) {
            global += local - mod;
        }

    }

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&d_matrix);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(int), &rows);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(int), &cols);
    checkError(err, "Setting kernel arguments");

    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, NULL,
        &global, &local,
        0, NULL, NULL);
    checkError(err, "enqueing");
    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clReleaseProgram(program);
    checkError(err, "releasing program");
    err = clReleaseKernel(kernel);
    checkError(err, "releasing program");

    return 0;


}


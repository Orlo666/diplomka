#define END_COLS 35

__kernel void isra(
   __global float *image,
   __global float *abundances,
   __global float *endmembers,
   __global float *em_em,
   __global float *em_x,
   const int orig_cols,
   const int orig_rows,
   const int end_cols,
   const int end_rows,
   const int iter,
   __local float *l_em_em
   ) {


    int gid_x = get_global_id(0);

    float numerator = 0.0;
    float numerator_s = 0.0;
    float denominator = 0.0;
    float denom[END_COLS];
    __private float p_abun[END_COLS];
    __private float p_em_x[END_COLS];
    int lid_x = get_local_id(0);

    if (lid_x < end_cols * end_cols) {
        l_em_em[lid_x] = em_em[lid_x];
    }
    for (int s=0; s<end_cols; s++) {
        p_abun[s] = abundances[s*orig_cols + gid_x];
        p_em_x[s] = em_x[s * orig_cols + gid_x];
    }

    barrier(CLK_LOCAL_MEM_FENCE);
    if (gid_x < orig_cols) {
        for (int i=0; i<iter; i++) {
            for (int end=0; end<end_cols; end++) {
                for (int end_x=0; end_x<end_cols; end_x++) {
                    denom[end] += l_em_em[end*end_cols + end_x] * p_abun[end_x];
                }
                p_abun[end] = p_abun[end] * (p_em_x[end] / denom[end]);
                denom[end] = 0.0;
            }

        }
        for (int s=0; s<end_cols; s++) {
            abundances[s*orig_cols + gid_x] = p_abun[s];
        }

    }
}

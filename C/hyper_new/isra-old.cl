#define END_ROWS 156
#define END_COLS 25

__kernel void isra(
   __global float *image,
   __global float *abundances,
   __global float *endmembers,
   const int orig_cols,
   const int orig_rows,
   const int end_cols,
   const int end_rows,
   const int iter
   ) {


    int gid_x = get_global_id(0);

    float numerator = 0.0;
    float denominator = 0.0;
    float dot = 0.0;
    __private float l_abun[END_COLS];
    int lid_x = get_local_id(0);

    __private float l_image[END_ROWS];
    __local float s_endmembers_cols[END_ROWS*END_COLS];
    float tmp_endmember;
    if (lid_x < end_rows) {
        for (int i=0; i<end_cols; i++) {
            s_endmembers_cols[lid_x*end_cols+i] = endmembers[lid_x*end_cols+i];
        }
    }
    for (int i=0; i<end_rows; i++) {
        l_image[i] = image[i*orig_cols + gid_x];
    }
    for (int s=0; s<end_cols; s++) {
        l_abun[s] = abundances[s*orig_cols + gid_x];
    }

    barrier(CLK_LOCAL_MEM_FENCE);
    if (gid_x < orig_cols) {
        for (int i=0; i<iter; i++) {
            for (int end=0; end<end_cols; end++) {
                for (int band=0; band<end_rows; band++) {
                    numerator = numerator + s_endmembers_cols[band*end_cols+end] * l_image[band];

                    for (int s=0; s<end_cols; s++) {
                        dot += s_endmembers_cols[band*end_cols+s] * l_abun[s];
                    }
                    denominator += dot*s_endmembers_cols[band*end_cols+end];
                    dot = 0.0;
                }

                l_abun[end] = l_abun[end] * (numerator / denominator);

                numerator = 0.0;
                denominator = 0.0;
            }
        }
        for (int s=0; s<end_cols; s++) {
            abundances[s*orig_cols + gid_x] = l_abun[s];
        }

    }
}


#ifndef PCA_H
#define PCA_H
#ifndef __MULT_HDR
#define __MULT_HDR

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <string.h>
#include <clBLAS.h>
#include <time.h>
#include <lapacke.h>
#include "eigen.h"
#include "utils.h"

#include <err_code.h>

extern char *getKernelSource(char *filename);
extern int output_device_info(cl_device_id );
extern double wtime();
extern int create_identity_matrix(double *matrix, int cols, int rows);
extern int qr_algorithm(double *matrix, int cols, int rows, double *eigen_values, double *eigen_vectors);



int qr_decomposition(double *matrix, int cols, int rows, double *matrix_r, double *matrix_q);
// Count Eigen values and eigen vectors of standard matrix
void eigen(double *orig_matrix, int orig_rows, double *eigen_values, double *eigen_vectors);

// Count PCA
int pca(double *image,int orig_rows,int orig_cols, cl_mem d_output, int *output_rows);
int get_matrix_q(double *orig_matrix, double *matrix_q, int cols, int rows);


#endif
#endif

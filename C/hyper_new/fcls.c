#include <fcls.h>

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#define WAVEFRONT 1024

extern int output_device_info(cl_device_id );
extern char *getKernelSource(char *filename);
extern double wtime();

void inverse(cl_float* A, long int N)
{
    long int *IPIV = malloc(N * sizeof(long int));
    long int LWORK = N*N;
    cl_float *WORK = malloc(LWORK * sizeof(int));
    long int INFO;

    sgetrf_(&N,&N,A,&N,IPIV,&INFO);
    sgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);

    free(IPIV);
    free(WORK);
}

int non_constrained(cl_mem d_abundances, cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int end_rows, int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id) {
    clblasOrder order = clblasRowMajor;
    cl_int err;
    cl_mem d_em_em_i;
    cl_mem d_em_em_em;
    cl_event event = NULL;
    cl_float *em_em_result = malloc(sizeof(cl_float) * end_cols * end_cols);
    cl_float *em_em_em_result = malloc(sizeof(cl_float) * end_cols * end_cols);
    int ret = 0;

    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    cl_float *em_em_b = malloc(sizeof(cl_float) * end_rows * end_cols);
    err = clEnqueueReadBuffer(queue, d_endmembers, CL_TRUE, 0,
        end_cols * end_rows * sizeof(cl_float), em_em_b, 0, NULL, NULL);
    checkError(err, "Reading abundances");
    clFinish(queue);
    save_matrix_to_file(em_em_b, end_rows, end_cols, "./out/fr/b_em.mat", "b_em");
    free(em_em_b);


    /*********************
     ****** EM' * EM ******
     *********************/
    d_em_em_i = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_cols * end_cols * sizeof(cl_float),
                          NULL, &err);

    checkError(err, "enqueing d_em_em");

    err = clblasSgemm(order, clblasTrans, clblasNoTrans, end_cols, end_cols, end_rows,
                      1.0, d_endmembers, 0, end_cols,
                      d_endmembers, 0, end_cols,
                      0.0, d_em_em, 0, end_cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        checkError(err, "returning for EM * EM");
        err = clEnqueueReadBuffer(queue, d_em_em, CL_TRUE, 0, end_cols * end_cols * sizeof(cl_float),
                                  em_em_result, 0, NULL, NULL);
        checkError(err, "Reading for em_em");
        err = clFinish(queue);
        checkError(err, "Finishing reading em_em");
    }

    /*****************************************
     ****** INVERSION OF em_em_result ********
     *****************************************/
    clFinish(queue);
    save_matrix_to_file(em_em_result, end_cols, end_cols, "./out/fr/t_b_inv_em_em.mat", "b_inv_em_em");

    inverse(em_em_result, (long int)end_cols);

    clFinish(queue);
    save_matrix_to_file(em_em_result, end_cols, end_cols, "./out/fr/t_inv_em_em.mat", "inv_em_em");


    err = clEnqueueWriteBuffer(queue, d_em_em_i, CL_TRUE, 0,
                               end_cols * end_cols * sizeof(cl_float), em_em_result, 0, NULL, NULL);
    checkError(err, "Writing inverse back to d_em_em");

    clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    /*****************************************
     ****** I(EM' * EM) * EM' ****************
     *****************************************/

    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }
    checkError(err, "creating queue");


    event = NULL;

    d_em_em_em = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_rows * end_cols * sizeof(cl_float),
                          NULL, &err);
    checkError(err, "creating buffer d_em_em_em");
    err = clEnqueueWriteBuffer(queue, d_em_em_em, CL_TRUE, 0,
                               end_rows * end_cols * sizeof(cl_float), em_em_em_result, 0, NULL, NULL);
    checkError(err, "enqueing d_em_em_em");

    err = clblasSgemm(order, clblasNoTrans, clblasTrans, end_cols, end_rows, end_cols,
                      1.0, d_em_em_i, 0, end_cols,
                      d_endmembers, 0, end_cols,
                      0.0, d_em_em_em, 0, end_rows, 1, &queue, 0, NULL, &event);
    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        checkError(err, "returning result of em_em * em_em_em");
    }

    clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);

    /*****************************************
     ************ EM_EM_EM * IMAGE ***********
     *****************************************/
    err = clblasSgemm(order, clblasNoTrans, clblasNoTrans, end_cols, cols, rows,
                      1.0, d_em_em_em, 0, end_rows,
                      d_image, 0, cols,
                      0.0, d_abundances, 0, cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        checkError(err, "returning result of em_em_em * image");
    }



    clReleaseEvent(event);
    clReleaseMemObject(d_em_em_i);
    clReleaseMemObject(d_em_em_em);
    clReleaseCommandQueue(queue);
    free(em_em_result);
    free(em_em_em_result);

    return 0;
}

int non_constrained_simple(cl_mem d_abundances, cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int end_rows, int end_cols, cl_context ctx, cl_device_id device_id) {
    cl_event event = NULL;
    clblasOrder order = clblasRowMajor;
    int ret = 0;
    cl_int err;
    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        return 1;
    }

    err = clblasSgemm(order, clblasTrans, clblasNoTrans, end_cols, cols, end_rows,
                      1.0, d_endmembers, 0, end_cols,
                      d_image, 0, cols,
                      0.0, d_abundances, 0, cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    } else {
        err = clWaitForEvents(1, &event);
        checkError(err, "returning result EM * IMAGE");
    }
    clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    return 0;
}



int isra(cl_mem d_abundances, cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int end_rows, int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id) {
    cl_int err;
    cl_command_queue queue = 0;
    cl_event event = NULL;
    cl_int iter = 100;
    cl_program program;       // compute program
    cl_kernel kernel;
    clblasOrder order = clblasRowMajor;
    cl_mem d_em_x;
    int ret = 0;

    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating queue");


    /*********************
     ****** EM' * X ******
     *********************/

    d_em_x = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_cols * cols * sizeof(cl_float),
                          NULL, &err);
    checkError(err, "Creating buffer d_em_x");

    err = clblasSgemm(order, clblasTrans, clblasNoTrans, end_cols, cols, end_rows,
                      1.0, d_endmembers, 0, end_cols,
                      d_image, 0, cols,
                      0.0, d_em_x, 0, cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        checkError(err, "returning for EM * x");
    }

    //cl_float *em_em = malloc(sizeof(cl_float) * end_cols * end_cols);
    //err = clEnqueueReadBuffer(queue, d_em_em, CL_TRUE, 0,
    //    end_cols * end_cols * sizeof(cl_float), em_em, 0, NULL, NULL);
    //checkError(err, "Reading abundances");
    //clFinish(queue);
    //save_matrix_to_file(em_em, end_cols, end_cols, "./out/fr/t_em_em.mat", "em_em");
    //free(em_em);

    //cl_float *abundances = malloc(sizeof(cl_float) * end_cols * cols);
    //err = clEnqueueReadBuffer(queue, d_abundances, CL_TRUE, 0,
    //    end_cols * cols * sizeof(cl_float), abundances, 0, NULL, NULL);
    //checkError(err, "Reading abundances");
    //clFinish(queue);
    //save_matrix_to_file(abundances, end_cols, cols, "./out/fr/t_abun.mat", "abun");
    //free(abundances);

    //cl_float *endmembers = malloc(sizeof(cl_float) * end_rows * end_cols);
    //err = clEnqueueReadBuffer(queue, d_endmembers, CL_TRUE, 0,
    //    end_cols * end_rows * sizeof(cl_float), endmembers, 0, NULL, NULL);
    //checkError(err, "Reading abundances");
    //clFinish(queue);
    //save_matrix_to_file(endmembers, end_rows, end_cols, "./out/fr/t_end.mat", "em");
    //free(endmembers);


    //char *old_isra_kernel = getKernelSource("../../hyper_new/isra-old.cl");
    char *isra_kernel = getKernelSource("../../hyper_new/isra.cl");
    //cl_float *old_output = malloc(sizeof(cl_float) * end_cols * cols);
    //cl_float *new_output = malloc(sizeof(cl_float) * end_cols * cols);
    //cl_mem d_old_abundances = clCreateBuffer(ctx, CL_MEM_READ_WRITE,
    //    end_cols * cols * sizeof(cl_float), NULL, &err);
    //TODO COPY


    /* Setup OpenCL environment. */
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }

    program = clCreateProgramWithSource(ctx, 1, (const char **) & isra_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "isra", &err);
    checkError(err, "Creating kernel");


    // CHECK input of ISRA
    //cl_float *image = malloc(sizeof(cl_float) * rows * cols);
    //err = clEnqueueReadBuffer(queue, d_image, CL_TRUE, 0,
    //    cols * rows * sizeof(cl_float), image, 0, NULL, NULL);
    //checkError(err, "Reading d_endmembers");
    //clFinish(queue);
    //save_matrix_to_file(image, rows, cols, "./d_image.mat", "d_image");

    //cl_float *abundances = malloc(sizeof(cl_float) * rows * cols);
    //err = clEnqueueReadBuffer(queue, d_abundances, CL_TRUE, 0,
    //    cols * end_cols * sizeof(cl_float), abundances, 0, NULL, NULL);
    //checkError(err, "Reading d_endmembers");
    //clFinish(queue);
    //save_matrix_to_file(abundances, end_rows, end_cols, "./d_abundances.mat", "d_abundances");

    //printf("%d - %d\n", end_rows, end_cols);
    //cl_float *endmembers = malloc(sizeof(cl_float) * end_rows * end_cols);
    //err = clEnqueueReadBuffer(queue, d_endmembers, CL_TRUE, 0,
    //    end_rows * end_cols * sizeof(cl_float), endmembers, 0, NULL, NULL);
    //checkError(err, "Reading d_endmembers");
    //clFinish(queue);
    //save_matrix_to_file(endmembers, end_rows, end_cols, "./d_endmembers.mat", "d_endmembers");

    //clEnqueueCopyBuffer(queue, d_abundances, d_old_abundances, 0, 0, cols * end_cols * sizeof(cl_float), 0, 0, &event);
    //err = clFinish(queue);

    size_t global;
    int mod = cols % WAVEFRONT;
    size_t global_isra = cols;
    if (mod > 0) {
        global_isra += WAVEFRONT - mod;
    }
    const size_t local_isra = WAVEFRONT;

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_image);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_abundances);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_endmembers);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_em_em);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_em_x);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 5, sizeof(cols), &cols);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 6, sizeof(int), &rows);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 7, sizeof(int), &end_cols);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 8, sizeof(int), &end_rows);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 9, sizeof(cl_int), &iter);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 10, sizeof(cl_float) * end_cols * end_cols, NULL);
    checkError(err, "Setting kernel arguments");




    double time = wtime();
    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, 0,
        &global_isra, &local_isra,
        0, NULL, NULL);
    checkError(err, "Enqueing Range Kernel");
    err = clFinish(queue);
    checkError(err, "Finishing queue");
    //double end_time = wtime() - time;
    //printf("TIME NEW: %f\n", end_time);
    //printf("finished main isra\n");



    //err = clEnqueueCopyBuffer(queue, bufAbundances, CL_TRUE, 0, cols * end_cols * sizeof(cl_float),
    //                          abundances, 0, NULL, NULL);

    clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    err = clReleaseKernel(kernel);

    clReleaseMemObject(d_em_x);
    free(isra_kernel);

    return 0;
}

int sum_to_one(cl_mem d_abundances, int abun_rows, int abun_cols, cl_context ctx, cl_device_id device_id) {
    cl_int err;
    cl_event event = NULL;
    cl_program program;       // compute program
    int ret = 0;
    cl_kernel kernel;

    char *sum_to_one_kernel = getKernelSource("../../hyper_new/sum_to_one.cl");
    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating queue");

    /* Setup OpenCL environment. */
    program = clCreateProgramWithSource(ctx, 1, (const char **) & sum_to_one_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "sum_to_one", &err);
    checkError(err, "Creating kernel");

    int mod = abun_cols % WAVEFRONT;
    size_t global_sum = abun_cols;
    if (mod > 0) {
        global_sum += WAVEFRONT - mod;
    }
    const size_t local_sum = WAVEFRONT;
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_abundances);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(int), &abun_rows);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(int), &abun_cols);
    checkError(err, "Setting kernel arguments");

    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, NULL,
        &global_sum, &local_sum,
        0, NULL, NULL);

    checkError(err, "Enqueueing kernel");
    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clReleaseProgram(program);
    err = clReleaseKernel(kernel);
    return 0;
}



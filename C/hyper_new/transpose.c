#include <transpose.h>

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#define WAVEFRONT 512
#define BLOCK_DIM 16

int transpose(cl_mem d_matrix, cl_mem d_out_matrix, int rows, int cols, cl_context ctx, cl_device_id device_id) {
    cl_int err;
    cl_event event = NULL;
    cl_program program;       // compute program
    int ret = 0;
    cl_kernel kernel;

    char *sum_to_one_kernel = getKernelSource("../../hyper_new/transpose.cl");
    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating queue");

    /* Setup OpenCL environment. */
    program = clCreateProgramWithSource(ctx, 1, (const char **) & sum_to_one_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "transpose", &err);
    checkError(err, "Creating kernel");

    size_t global[] = {cols, rows};
    size_t local[] = {BLOCK_DIM, BLOCK_DIM};
    int mod = cols % BLOCK_DIM;
    if (mod > 0) {
        global[0] += BLOCK_DIM - mod;
    }
    mod = rows % BLOCK_DIM;
    if (mod > 0) {
        global[1] += BLOCK_DIM - mod;
    }
    size_t offset = 0;

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&d_out_matrix);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&d_matrix);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(int), &offset);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 3, sizeof(int), &cols);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 4, sizeof(int), &rows);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 5, (BLOCK_DIM + 1) * BLOCK_DIM * sizeof(cl_float), 0 );
    checkError(err, "Setting kernel arguments");


    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        2, NULL,
        global, local,
        0, NULL, NULL);
    checkError(err, "enqueing");
    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clReleaseProgram(program);
    checkError(err, "releasing program");
    err = clReleaseKernel(kernel);
    checkError(err, "releasing program");

    return 0;


}



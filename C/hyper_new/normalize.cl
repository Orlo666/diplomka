__kernel void normalize_matrix(__global float *matrix, int rows, int cols) {
    unsigned int gid_x = get_global_id(0);
    if (gid_x < cols) {
        float tmp;
        float size = 0;
        for (int i = 0; i < rows; i++) {
            tmp = matrix[i*cols + gid_x];
            size += pown(tmp, 2);
        }
        size = sqrt(size);
        for (int i = 0; i < rows; i++) {
            matrix[i*cols + gid_x] = matrix[i*cols + gid_x] / size;
        }
    }
}

__kernel void normalize_matrix_by_band(__global float *matrix, int rows, int cols) {
    unsigned int gid_x = get_global_id(0);
    if (gid_x < cols) {
        float tmp;
        float size = 0;
        for (int i = 0; i < cols; i++) {
            tmp = matrix[gid_x*cols + i];
            size += pown(tmp, 2);
        }
        size = sqrt(size);
        for (int i = 0; i < cols; i++) {
            matrix[gid_x*cols + i] = matrix[gid_x*cols + i] / size;
        }
    }
}


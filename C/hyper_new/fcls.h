#ifndef FCLS_H
#define FCLS_H
#ifndef __MULT_HDR
#define __MULT_HDR

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <string.h>
#include <clBLAS.h>
#include <time.h>
#include <lapacke.h>

#include "err_code.h"
#include "utils.h"


/*
 * E: endmembers
 * (E'*E)*E'
 */
int non_constrained(cl_mem d_abundances, cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int end_rows, int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id);

/*
 * E: endmembers
 * E'*E
 */
int non_constrained_simple(cl_mem d_abundances, cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int end_rows, int end_cols, cl_context ctx, cl_device_id device_id);

/*
 * E: endmembers
 * x: original vector of img
 * PHI^(k+1) = PHI^k * ((E'*x) / (E'*E*PHI^k))
 */
int isra(cl_mem d_abundances, cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int end_rows, int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id);

/*
 */
int sum_to_one(cl_mem d_abundances, int abun_cols, int abun_rows, cl_context ctx, cl_device_id device_id);

#endif
#endif

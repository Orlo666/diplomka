#include "ppi.h"

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#define MAX_THREADS 1024

extern char *getKernelSource(char *filename);
extern double wtime();

unsigned int nextPow2(unsigned int x)
{
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}

int generate_random_skewers(cl_mem d_skewers, int rows, int cols,
    cl_context ctx, cl_device_id device_id) {
    cl_int err;
    cl_command_queue queue = 0;
    cl_event event = NULL;
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    cl_float *skewers = malloc(sizeof(cl_float) * rows * cols);
    for (int i = 0; i<rows*cols; i++) {
        skewers[i] = (cl_float)rand()/(cl_float)rand();
    }
    err = clEnqueueWriteBuffer(queue, d_skewers, CL_TRUE, 0,
        cols * rows * sizeof(cl_float), skewers, 0, NULL, NULL);
    clFinish(queue);
    free(skewers);
}


int ppi(cl_mem d_image, int rows, int cols, cl_mem d_endmembers, int q, cl_mem d_skewers, int iter,
        cl_context ctx, cl_device_id device_id) {

    double start_time = wtime();
    double run_time;
    int mod;
    cl_int *max_idx_array = malloc(iter * sizeof(cl_int));
    int *end_members_indexes = malloc(sizeof(int) * q);
    int index = 0;
    int *votes = malloc(sizeof(int) * cols);
    for (int i=0; i<cols; i++) {
        votes[i] = 0;
    }

    char *max_indexes_kernel = getKernelSource("../../hyper_new/max_indexes.cl");
    char *get_endmembers_kernel = getKernelSource("../../hyper_new/get_endmembers.cl");
    clblasOrder order = clblasRowMajor;
    cl_command_queue queue = 0;


    // init OPENCL
    cl_program program;       // compute program
    cl_kernel kernel;
    cl_mem d_max_idx_array;
    cl_mem d_image_skewers;
    int err;
    cl_event event = NULL;
    int ret = 0;

    // Fill vectors a and b with random float values


    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");

    /***************************************
     * IMAGE * SKEWERS
     **************************************/
    start_time = wtime();

    //image_skewers = malloc(sizeof(cl_float) * iter * cols);
    d_image_skewers = clCreateBuffer(ctx, CL_MEM_READ_WRITE, cols * iter * sizeof(cl_float),
        NULL, &err);
    checkError(err, "Createing Buffer d_image_skewers");
    //TODO delete
    //err = clEnqueueWriteBuffer(queue, d_image_skewers, CL_TRUE, 0,
    //    cols * iter * sizeof(cl_float), result, 0, NULL, NULL);
    //checkError(err, "Filling d_image_skewers");

    err = clblasSgemm(order, clblasTrans, clblasNoTrans, cols, iter, rows,
                      1.0, d_image, 0, cols,
                      d_skewers, 0, iter,
                      0.0, d_image_skewers, 0, iter, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemm() failed with %d\n", err);
        ret = 1;
        return ret;
    } else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        checkError(err, "returning result");
    }


    //clReleaseEvent(event);
    //clReleaseCommandQueue(queue);

    /***************************************************
     * GET MAX IDX OF SKEWERS
     ***************************************************/
    start_time = wtime();

    program = clCreateProgramWithSource(ctx, 1, (const char **) & max_indexes_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "max_indexes_reduction", &err);
    checkError(err, "Creating kernel_reduce_end");

    size_t local_max_idx[] = {8, 128};
    size_t global_max_idx_x = cols;
    size_t global_max_idx_y = iter;
    mod = cols % local_max_idx[0];
    if (mod > 0) {
        global_max_idx_x += local_max_idx[0] - mod;
    }
    int max_cols = cols;
    int blocks = global_max_idx_x/local_max_idx[0];
    d_max_idx_array = clCreateBuffer(ctx, CL_MEM_READ_WRITE, iter * blocks * sizeof(int), NULL, &err);
    checkError(err, "Creating buffer d_max_idx_array");

    mod = iter % local_max_idx[1];
    if (mod > 0) {
        global_max_idx_y += local_max_idx[1] - mod;
    }

    int first_run = 1;

    size_t global_max_idx[] = {local_max_idx[0], global_max_idx_y};
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_image_skewers);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_max_idx_array);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 2, sizeof(int), &max_cols);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 3, sizeof(int), &iter);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 4, sizeof(int), &blocks);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 5, sizeof(int), &first_run);
    checkError(err, "Setting kernel_reduce_init arguments");
    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        2, 0,
        global_max_idx, local_max_idx,
        0, NULL, NULL);
    checkError(err, "Enqueuing max index kernel");

    // END OF FIRST RUN
    first_run = 0;
    blocks = local_max_idx[0];
    local_max_idx[0] = 1;
    if (iter < MAX_THREADS) {
        local_max_idx[1] = iter;
        global_max_idx_y = iter;
    } else {
        local_max_idx[1] = MAX_THREADS;
        mod = iter % local_max_idx[1];
        if (mod > 0) {
            global_max_idx_y += local_max_idx[1] - mod;
        }
    }

    global_max_idx[0] = 1;
    global_max_idx[1] = global_max_idx_y;

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_image_skewers);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_max_idx_array);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 2, sizeof(int), &max_cols);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 3, sizeof(int), &iter);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 4, sizeof(int), &blocks);
    checkError(err, "Setting kernel_reduce_init arguments");
    err |= clSetKernelArg(kernel, 5, sizeof(int), &first_run);
    checkError(err, "Setting kernel_reduce_init arguments");
    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        2, 0,
        global_max_idx, local_max_idx,
        0, NULL, NULL);
    checkError(err, "Enqueuing max index kernel");


    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clEnqueueReadBuffer(queue, d_max_idx_array, CL_TRUE, 0, iter*sizeof(cl_int),
                              max_idx_array, 0, NULL, NULL);
    checkError(err, "Getting max_idx back from kernel");


    clFinish(queue);

    for (int i=0; i<iter; i++) {
        votes[max_idx_array[i]]++;
    }

    err = clReleaseMemObject(d_image_skewers);
    checkError(err, "Releasing d_image_skewers");
    err = clReleaseKernel(kernel);
    checkError(err, "Releasing kernel");
    err = clReleaseMemObject(d_max_idx_array);
    checkError(err, "Releasing d_max_idx_array");
    free(max_idx_array);
    err = clReleaseProgram(program);
    checkError(err, "Releasing Program");

    /***************************************************
     * COMPUTING VOTES AND ENDMEMBERS
     ***************************************************/
    start_time = wtime();

    int max = 0;
    int max_index = 0;
    for (int i=0; i<q; i++) {
        max = 0;
        max_index = 0;
        for (int j=0; j<cols; j++) {
            if (max < votes[j]) {
                max = votes[j];
                max_index = j;
            }
        }
        votes[max_index] = 0;
        end_members_indexes[i] = max_index;
    }

    /**************************************
     * COPY ENDMEMBERS TO DEV MEMORY
     **************************************/

    program = clCreateProgramWithSource(ctx, 1, (const char **) &get_endmembers_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }

    kernel = clCreateKernel(program, "get_endmembers", &err);
    checkError(err, "Creating kernel");

    cl_mem d_endmembers_max_indexes = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
        q * sizeof(int), end_members_indexes, &err);
    checkError(err, "Creating buffer d_endmembers_max_indexes");

    //err = clEnqueueWriteBuffer(queue, d_endmembers_max_indexes, CL_TRUE, 0,
    //    q * sizeof(int), , 0, NULL, NULL);
    //checkError(err, "Writing into d_endmembers_max_indexes");

    int ge_threads = 16;
    if (ge_threads > q) {
        ge_threads = q;
    }
    mod = rows % ge_threads;
    int global_first = rows;
    if (mod > 0) {
        global_first = rows + ge_threads - mod;
    }
    int global_second = q;
    if (q > ge_threads) {
        mod = q % ge_threads;
        if (mod > 0) {
            global_second = q + ge_threads - mod;
        }
    }

    const size_t global_endmembers[2] = {global_first, global_second};
    const size_t work_item_endmembers[2] = {ge_threads, ge_threads};

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_image);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_endmembers);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_endmembers_max_indexes);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 3, sizeof(rows), &rows);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 4, sizeof(cols), &cols);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 5, sizeof(q), &q);
    checkError(err, "Setting kernel arguments");

    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        2, 0,
        global_endmembers, work_item_endmembers,
        0, NULL, NULL);
    checkError(err, "clEnqueueNDRangeKernel");
    err = clFinish(queue);
    checkError(err, "Finishing queue");

    err = clReleaseMemObject(d_endmembers_max_indexes);
    checkError(err, "Releasing d_endmembers_max_indexes");
    err = clReleaseKernel(kernel);
    checkError(err, "Releasing kernel");
    err = clReleaseCommandQueue(queue);
    checkError(err, "Releasing queue");
    err = clReleaseProgram(program);
    checkError(err, "Releasing Program");


    free(end_members_indexes);
    //free(result);
    free(votes);
    free(max_indexes_kernel);
    free(get_endmembers_kernel);
    return 0;
}

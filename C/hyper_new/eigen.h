#ifndef PCA_H
#define PCA_H
#ifndef __MULT_HDR
#define __MULT_HDR

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <string.h>
#include <clBLAS.h>
#include <time.h>
#include <lapacke.h>
#include <stdbool.h>

#include <err_code.h>

int qr_algorithm(double *matrix, int cols, int rows, double *eigen_values, double *eigen_vectors);
int qr_decomposition(double *matrix, int cols, int rows, double *matrix_q, double *matrix_r);
#endif
#endif

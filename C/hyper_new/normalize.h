#ifndef NORM_H
#define NORM_H
#ifndef __MULT_HDR
#define __MULT_HDR

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <clBLAS.h>
#include "err_code.h"
#include "utils.h"

int normalize_matrix(cl_mem d_matrix, int rows, int cols, int by_band, cl_context ctx, cl_device_id device_id);

#endif
#endif

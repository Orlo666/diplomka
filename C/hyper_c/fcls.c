#include <fcls.h>

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

extern int output_device_info(cl_device_id );
extern char *getKernelSource(char *filename);
extern double wtime();

void inverse(double* A, long int N)
{
    long int *IPIV = malloc(N * sizeof(long int));
    long int LWORK = N*N;
    double *WORK = malloc(LWORK * sizeof(int));
    long int INFO;

    dgetrf_(&N,&N,A,&N,IPIV,&INFO);
    dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);

    free(IPIV);
    free(WORK);
}

int non_constrained(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols) {
    clblasOrder order = clblasRowMajor;
    cl_int err;
    cl_platform_id platform = 0;
    cl_device_id device_id = NULL;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
    cl_context ctx = 0;
    cl_command_queue queue = 0;
    cl_mem bufA, bufB, bufC;
    cl_mem bufX, bufY, bufZ;

    cl_event event = NULL;
    double *bckp_endmembers = malloc(end_rows * end_cols * sizeof(double));
    memcpy(bckp_endmembers, endmembers, end_rows * end_cols * sizeof(double));

    double *result = malloc(sizeof(double) * end_cols * end_cols);
    double *temp_result = malloc(sizeof(double) * end_cols * end_cols);

    int ret = 0;
    /* Setup OpenCL environment. */
    cl_uint numPlatforms;
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkError(err, "Finding platforms");
    if (numPlatforms == 0)
    {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    checkError(err, "Getting platforms");


    if (err != CL_SUCCESS) {
        printf( "clGetPlatformIDs() failed with %d\n", err );
        return 1;
    }

    for (int i = 0; i < numPlatforms; i++)
    {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS)
        {
            break;
        }
    }

    if (device_id == NULL)
        checkError(err, "Finding a device");

    ctx = clCreateContext(0, 1, &device_id, NULL, NULL, &err);

    if (err != CL_SUCCESS) {
        printf( "clCreateContext() failed with %d\n", err );
        return 1;
    }
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }
    /* Setup clblas. */
    err = clblasSetup();
    if (err != CL_SUCCESS) {
        printf("clblasSetup() failed with %d\n", err);
        clReleaseCommandQueue(queue);
        clReleaseContext(ctx);
        return 1;
    }


    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, end_rows * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer A");

    bufB = clCreateBuffer(ctx, CL_MEM_READ_ONLY, end_rows * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer B");

    bufC = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_cols * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer C");

    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
                               end_rows * end_cols * sizeof(double), endmembers, 0, NULL, NULL);
    checkError(err, "enqueing A");

    err = clEnqueueWriteBuffer(queue, bufB, CL_TRUE, 0,
                               end_rows * end_cols * sizeof(double), bckp_endmembers, 0, NULL, NULL);
    checkError(err, "enqueing B");

    err = clEnqueueWriteBuffer(queue, bufC, CL_TRUE, 0,
                               end_cols * end_cols * sizeof(double), result, 0, NULL, NULL);
    checkError(err, "enqueing C");


    err = clblasDgemm(order, clblasTrans, clblasNoTrans, end_cols, end_cols, end_rows,
                      1.0, bufA, 0, end_cols,
                      bufB, 0, end_cols,
                      0.0, bufC, 0, end_cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufC, CL_TRUE, 0, end_cols * end_cols * sizeof(double),
                                  result, 0, NULL, NULL);
        checkError(err, "returning result");
    }


    inverse(result, (long int)end_cols);



    clReleaseEvent(event);
    clReleaseMemObject(bufB);
    clReleaseMemObject(bufA);
    clReleaseMemObject(bufC);
    clReleaseCommandQueue(queue);
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }

    event = NULL;
    
    bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, end_cols * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer Z");

    bufY = clCreateBuffer(ctx, CL_MEM_READ_ONLY, end_rows * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer Y");

    bufZ = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_rows * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer Z");

    err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
                               end_cols * end_cols * sizeof(double), result, 0, NULL, NULL);
    checkError(err, "enqueing X");

    err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
                               end_rows * end_cols * sizeof(double), endmembers, 0, NULL, NULL);
    checkError(err, "enqueing Y");

    err = clEnqueueWriteBuffer(queue, bufZ, CL_TRUE, 0,
                               end_rows * end_cols * sizeof(double), temp_result, 0, NULL, NULL);
    checkError(err, "enqueing Z");

    err = clblasDgemm(order, clblasNoTrans, clblasTrans, end_cols, end_rows, end_cols,
                      1.0, bufX, 0, end_cols,
                      bufY, 0, end_cols,
                      0.0, bufZ, 0, end_rows, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufZ, CL_TRUE, 0, end_cols * end_rows * sizeof(double),
                                  temp_result, 0, NULL, NULL);
        checkError(err, "returning result");
    }

    clReleaseEvent(event);
    clReleaseMemObject(bufX);
    clReleaseMemObject(bufY);
    clReleaseMemObject(bufZ);
    clReleaseCommandQueue(queue);
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);

    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, end_cols * end_rows * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer A");

    bufB = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_cols * orig_rows * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer B");

    bufC = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_cols * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer C");

    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
                               end_cols * end_rows * sizeof(double), temp_result, 0, NULL, NULL);
    checkError(err, "enqueing A");

    err = clEnqueueWriteBuffer(queue, bufB, CL_TRUE, 0,
                               orig_rows * orig_cols * sizeof(double), input_matrix, 0, NULL, NULL);
    checkError(err, "enqueing B");

    err = clEnqueueWriteBuffer(queue, bufC, CL_TRUE, 0,
                               orig_cols * end_cols * sizeof(double), abundances, 0, NULL, NULL);
    checkError(err, "enqueing C");

    err = clblasDgemm(order, clblasNoTrans, clblasNoTrans, end_cols, orig_cols, orig_rows,
                      1.0, bufA, 0, end_rows,
                      bufB, 0, orig_cols,
                      0.0, bufC, 0, orig_cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufC, CL_TRUE, 0, orig_cols * end_cols * sizeof(double),
                                  abundances, 0, NULL, NULL);
        checkError(err, "returning result");
    }


    clReleaseEvent(event);
    clReleaseMemObject(bufA);
    clReleaseMemObject(bufB);
    clReleaseMemObject(bufC);
    clReleaseCommandQueue(queue);

    free(bckp_endmembers);
    free(result);
    free(temp_result);

    return 0;
}


int non_constrained_simple(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols) {
    clblasOrder order = clblasRowMajor;
    cl_int err;
    cl_platform_id platform = 0;
    cl_device_id device_id = NULL;
    cl_context ctx = 0;
    cl_command_queue queue = 0;
    cl_mem bufA, bufB, bufC;
    cl_mem bufX, bufY, bufZ;


    cl_event event = NULL;
    double *bckp_endmembers = malloc(end_rows * end_cols * sizeof(double));
    memcpy(bckp_endmembers, endmembers, end_rows * end_cols * sizeof(double));

    double *result = malloc(sizeof(double) * end_cols * end_cols);
    double *temp_result = malloc(sizeof(double) * end_cols * end_cols);

    int ret = 0;
    /* Setup OpenCL environment. */
    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkError(err, "Finding platforms");
    if (numPlatforms == 0)
    {
        printf("Found 0 platforms!\n");
        return EXIT_FAILURE;
    }

    // Get all platforms
    cl_platform_id Platform[numPlatforms];
    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
    checkError(err, "Getting platforms");

    // Secure a GPU
    for (int i = 0; i < numPlatforms; i++)
    {
        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
        if (err == CL_SUCCESS)
        {
            break;
        }
    }

    if (device_id == NULL)
        checkError(err, "Finding a device");

    err = output_device_info(device_id);
    checkError(err, "Printing device output");


    ctx = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateContext() failed with %d\n", err );
        return 1;
    }
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }
    /* Setup clblas. */
    err = clblasSetup();
    if (err != CL_SUCCESS) {
        printf("clblasSetup() failed with %d\n", err);
        clReleaseCommandQueue(queue);
        clReleaseContext(ctx);
        return 1;
    }


    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, end_rows * end_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer A");

    bufB = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer B");

    bufC = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_cols * orig_cols * sizeof(double),
                          NULL, &err);
    checkError(err, "creating buffer C");

    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
                               end_rows * end_cols * sizeof(double), endmembers, 0, NULL, NULL);
    checkError(err, "enqueing A");

    err = clEnqueueWriteBuffer(queue, bufB, CL_TRUE, 0,
                               orig_rows * orig_cols * sizeof(double), input_matrix, 0, NULL, NULL);
    checkError(err, "enqueing B");

    err = clEnqueueWriteBuffer(queue, bufC, CL_TRUE, 0,
                               orig_cols * end_cols * sizeof(double), abundances, 0, NULL, NULL);
    checkError(err, "enqueing C");


    err = clblasDgemm(order, clblasTrans, clblasNoTrans, end_cols, orig_cols, end_rows,
                      1.0, bufA, 0, end_cols,
                      bufB, 0, orig_cols,
                      0.0, bufC, 0, orig_cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufC, CL_TRUE, 0, orig_cols * end_cols * sizeof(double),
                                  abundances, 0, NULL, NULL);
        checkError(err, "returning result");
    }


    clReleaseEvent(event);
    clReleaseMemObject(bufA);
    clReleaseMemObject(bufB);
    clReleaseMemObject(bufC);
    clReleaseCommandQueue(queue);
    clReleaseContext(ctx);
    
    free(bckp_endmembers);
    free(result);
    free(temp_result);

    return 0;

}



int isra(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols) {
    cl_int err;
    cl_platform_id platform = 0;
    cl_device_id device_id = NULL;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
    cl_context ctx = 0;
    cl_command_queue queue = 0;
    cl_mem bufAbundances, bufEndmembers, bufImage, bufOutput;
    cl_event event = NULL;
    cl_int iter = 1000;
    cl_program program;       // compute program
    cl_kernel kernel;


    char *isra_kernel = getKernelSource("../../hyper_c/isra.cl");
    double *result = malloc(sizeof(double) * end_cols * end_cols);
    double *temp_result = malloc(sizeof(double) * end_cols * end_rows);
    double *output = malloc(sizeof(double) * end_cols * orig_cols);

    int ret = 0;
    /* Setup OpenCL environment. */
    err = clGetPlatformIDs(1, &platform, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetPlatformIDs() failed with %d\n", err );
        return 1;
    }
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetDeviceIDs() failed with %d\n", err );
        return 1;
    }
    props[1] = (cl_context_properties)platform;
    ctx = clCreateContext(props, 1, &device_id, NULL, NULL, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateContext() failed with %d\n", err );
        return 1;
    }
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }
    program = clCreateProgramWithSource(ctx, 1, (const char **) & isra_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "isra", &err);
    checkError(err, "Creating kernel");

    bufImage = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, orig_cols * orig_rows * sizeof(double), input_matrix, &err);
    checkError(err, "Creating buffer bufImage");

    bufEndmembers = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, end_cols * end_rows * sizeof(double), endmembers, &err);
    checkError(err, "Creating buffer bufEndmembers");

    bufAbundances = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, orig_cols * end_cols * sizeof(double), abundances, &err);
    checkError(err, "Creating buffer bufAbundances");

    bufOutput = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, orig_cols * end_cols * sizeof(double), output, &err);
    checkError(err, "Creating buffer bufAbundances");

    size_t global = orig_cols;
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufImage);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &bufAbundances);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(cl_mem), &bufEndmembers);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 3, sizeof(cl_mem), &bufOutput);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 4, sizeof(int), &orig_cols);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 5, sizeof(int), &orig_rows);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 6, sizeof(int), &end_cols);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 7, sizeof(int), &end_rows);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 8, sizeof(cl_int), &iter);
    checkError(err, "Setting kernel arguments");


    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, NULL,
        &global, NULL,
        0, NULL, NULL);

    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clEnqueueReadBuffer(queue, bufAbundances, CL_TRUE, 0, orig_cols * end_cols * sizeof(double),
                              abundances, 0, NULL, NULL);
    checkError(err, "Getting max_idx back from kernel");

    clReleaseEvent(event);
    clReleaseMemObject(bufImage);
    clReleaseMemObject(bufAbundances);
    clReleaseMemObject(bufEndmembers);
    clReleaseMemObject(bufOutput);
    clReleaseCommandQueue(queue);
    err = clReleaseKernel(kernel);
    
    // Free Memmory
    free(result);
    free(temp_result);
    free(output);
 
    return 0;
}

int sum_to_one(double *abundances, int abun_cols, int abun_rows) {
    cl_int err;
    cl_platform_id platform = 0;
    cl_device_id device_id = NULL;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };
    cl_context ctx = 0;
    cl_command_queue queue = 0;
    cl_mem bufAbundances;
    cl_event event = NULL;
    cl_program program;       // compute program
    cl_kernel kernel;


    char *sum_to_one_kernel = getKernelSource("../../hyper_c/sum_to_one.cl");

    int ret = 0;
    /* Setup OpenCL environment. */
    err = clGetPlatformIDs(1, &platform, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetPlatformIDs() failed with %d\n", err );
        return 1;
    }
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetDeviceIDs() failed with %d\n", err );
        return 1;
    }
    props[1] = (cl_context_properties)platform;
    ctx = clCreateContext(props, 1, &device_id, NULL, NULL, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateContext() failed with %d\n", err );
        return 1;
    }
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }
    program = clCreateProgramWithSource(ctx, 1, (const char **) & sum_to_one_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "sum_to_one", &err);
    checkError(err, "Creating kernel");


    bufAbundances = clCreateBuffer(ctx, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, abun_rows * abun_cols * sizeof(double), abundances, &err);
    checkError(err, "Creating buffer bufAbundances");

    size_t global = abun_cols;
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufAbundances);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(int), &abun_rows);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(int), &abun_cols);
    checkError(err, "Setting kernel arguments");

    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, NULL,
        &global, NULL,
        0, NULL, NULL);

    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clEnqueueReadBuffer(queue, bufAbundances, CL_TRUE, 0, abun_rows * abun_cols * sizeof(double),
                              abundances, 0, NULL, NULL);
    checkError(err, "Getting max_idx back from kernel");
    err = clReleaseProgram(program);
    err = clReleaseKernel(kernel);

   
    return 0;
}



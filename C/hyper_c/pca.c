#include "pca.h"
#include <cblas.h>

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif


void eigen(double *orig_matrix, int orig_rows, double *eigen_values, double *eigen_vectors) {
    double *copy_orig_matrix = malloc(sizeof(double) * orig_rows * orig_rows);
    memcpy(copy_orig_matrix, orig_matrix, sizeof(double) * orig_rows * orig_rows);
    int n = orig_rows;
    int lda = orig_rows;
    int info;
    int lwork = -1;
    double wkopt;
    double* work;
    /* Local arrays */
    int N = orig_rows;
    int LDVL = N;
    int ldvl = N;
    int ldvr = N;
    int LDVR = N;
    int LDA = N;
    double wi[N], vl[LDVL*N];
    /* Solve eigenproblem */
    info = LAPACKE_dgeev( LAPACK_ROW_MAJOR, 'V', 'V', n, copy_orig_matrix, lda, eigen_values, wi,
                    vl, ldvl, eigen_vectors, ldvr );
    // dsyev( "Vectors", "Upper", &n, a, &lda, w, &wkopt, &lwork, &info );
    if( info > 0 ) {
        printf( "The algorithm failed to compute eigenvalues.\n" );
        exit( 1 );
    }
    free(copy_orig_matrix);
    /* Print eigenvalues */
//    print_eigenvalues( "Eigenvalues", n, eigen_values, wi );
//    char* output_file = "../../../data/Data_MAT/C_scatter.mat";
//    save_matrix_to_file(eigen_values, 1, orig_rows, output_file, "e_values");
//    /* Print eigenvectors */
//    print_eigenvectors( "Eigenvectors (stored columnwise)", n, eigen_values, vl, ldvl );
//    /* Print eigenvectors */
//    print_eigenvectors( "Eigenvectors (stored columnwise)", n, eigen_values, eigen_vectors, ldvr );
}

int pca(double *image,int orig_rows,int orig_cols, double **output, int *output_rows) {
    //Compute the d-dimensional mean vector
    double *image_mean = malloc(sizeof(double) * orig_rows);
    double *normalized_image = malloc(sizeof(double) * orig_rows * orig_cols);

    for (int i = 0; i<orig_rows; i++) {
        image_mean[i] = 0.0;
        for (int j = 0; j<orig_cols; j++) {
            image_mean[i] += image[i*orig_cols + j];
        }
        image_mean[i] = image_mean[i] / orig_cols;
    }

    // Computing the Scatter Matrix
    double start_time;
    double run_time;
    start_time = wtime();

    // clBLAS init
    clblasOrder order = clblasRowMajor;

    // init OPENCL
    cl_device_id     device_id;     // compute device id
    cl_context       ctx;       // compute context
    cl_command_queue queue;      // compute command queue
    cl_program       program;       // compute program
    cl_kernel        kernel;

    cl_mem bufA, bufX, bufY;
    cl_int err;
    cl_event event = NULL;
    int ret = 0;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };


    // Fill vectors a and b with random float values

    cl_uint numPlatforms;

    // Find number of platforms
    err = clGetPlatformIDs(0, NULL, &numPlatforms);
    checkError(err, "Finding platforms");
    if (numPlatforms == 0)
    {
        printf("Found 0 platforms!\n");
        free(image_mean);
        return EXIT_FAILURE;
    }

    // Get all platforms
//    cl_platform_id Platform[numPlatforms];
//    err = clGetPlatformIDs(numPlatforms, Platform, NULL);
//    checkError(err, "Getting platforms");
//
//    // Secure a GPU
//    for (int i = 0; i < numPlatforms; i++)
//    {
//        err = clGetDeviceIDs(Platform[i], DEVICE, 1, &device_id, NULL);
//        if (err == CL_SUCCESS)
//        {
//            break;
//        }
//    }
//
//    if (device_id == NULL)
//        checkError(err, "Finding a device");
//
//    err = output_device_info(device_id);
//    checkError(err, "Printing device output");
    cl_platform_id platform = 0;

    err = clGetPlatformIDs(1, &platform, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetPlatformIDs() failed with %d\n", err );
        free(image_mean);
        return 1;
    }
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetDeviceIDs() failed with %d\n", err );
        free(image_mean);
        return 1;
    }
    props[1] = (cl_context_properties)platform;


    // Create a compute context
    ctx = clCreateContext(props, 1, &device_id, NULL, NULL, &err);


//    ctx = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
    checkError(err, "Creating context");

    // Create a command queue
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");

    /**************************************
    * clBLAS
    *************************************/
    // clBLAS Setup
    err = clblasSetup();
    if (err != CL_SUCCESS) {
        printf("clblasSetup() failed with %d\n", err);
        clReleaseCommandQueue(queue);
        clReleaseContext(ctx);
        free(image_mean);
        return 1;
    }

    //run_time = wtime() - start_time;
    puts("INIT POINTERS and INIT OPENCL");
    printf("%f s\n", run_time);

    //start_time = wtime();
    double *temp = malloc(sizeof(double) * orig_rows * orig_rows);
    double *scatter_matrix = malloc(sizeof(double) * orig_rows * orig_rows);
    for (int i=0; i<orig_rows*orig_rows; i++) {
        scatter_matrix[i] = 0.0;
    }


    bufY = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(double),
                              NULL, &err);
    bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(double),
                              NULL, &err);
    bufA = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_rows * orig_rows * sizeof(double),
                              NULL, &err);


    memcpy(normalized_image, image, sizeof(double) * orig_cols * orig_rows);
    for (int i=0; i<orig_cols; i++) {
        for (int j=0; j<orig_rows; j++) {
            normalized_image[j*orig_cols + i] - image_mean[j];
        }
    }
    double *normalized_image2 = malloc(sizeof(double) *orig_cols * orig_rows);
    memcpy(normalized_image2, normalized_image, sizeof(double) * orig_cols * orig_rows);


    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");

    err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
        orig_rows * orig_cols * sizeof(double), normalized_image, 0, NULL, NULL);

    err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
        orig_rows * orig_cols * sizeof(double), normalized_image2, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
        orig_rows * orig_rows * sizeof(double), scatter_matrix, 0, NULL, NULL);
    err = clblasDgemm(order, clblasNoTrans, clblasTrans, orig_rows, orig_rows, orig_cols,
                      1.0, bufY, 0, orig_cols,
                      bufX, 0, orig_cols,
                      0.0, bufA, 0, orig_rows, orig_rows, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("Scatter matrix clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufA, CL_TRUE, 0, orig_rows * orig_rows * sizeof(double),
                                  scatter_matrix, 0, NULL, NULL);
        checkError(err, "returning result");
    }
    clReleaseMemObject(bufA);
    clReleaseMemObject(bufX);
    clReleaseMemObject(bufY);




    free(temp);
    run_time = wtime() -  start_time;
    printf("SCATTER MATRIX...\n");
    printf("%f s\n", run_time);
    /***************************************************
    * EigenValues
    ***************************************************/

    double *eigen_values = malloc(sizeof(double) * orig_rows);
    double *eigen_vectors = malloc(sizeof(double) * orig_rows * orig_rows);

    eigen(scatter_matrix, orig_rows, eigen_values, eigen_vectors);

    //double *eigen_values_matrix = malloc(sizeof(double) * orig_rows * orig_rows);
    //double *eigen_vectors = malloc(sizeof(double) * orig_rows * orig_rows);
    //double *eigen_values = malloc(sizeof(double) * orig_rows);

    //create_identity_matrix(eigen_vectors, orig_rows, orig_rows);
    //puts("AAAAAAAAAA");
    //qr_algorithm(scatter_matrix, orig_rows, orig_rows, eigen_values_matrix, eigen_vectors);

    //for (int i=0; i<orig_rows; i++) {
    //    eigen_values[i] = eigen_values_matrix[i*orig_rows + i];
    //}

    int bottom_index = orig_rows;
    for (int i=0; i<orig_rows; i++ ) {
        if (eigen_values[i] < 1.0) {
            bottom_index = i;
            break;
        }
    }
    // take vectors
    double *final_eigen_vectors = malloc(sizeof(double) * orig_rows * bottom_index);
    for (int i=0; i<bottom_index; i++) {
        for (int j=0; j<orig_rows; j++) {
            final_eigen_vectors[j*bottom_index + i] = eigen_vectors[j*orig_rows + i];
        }
    }

    double *temp_output = malloc(sizeof(double) * orig_cols * bottom_index);
    *output_rows = bottom_index;

    // promitnuti
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");
    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * bottom_index * sizeof(double),
                          NULL, &err);
    bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(double),
                          NULL, &err);
    bufY = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_cols * bottom_index * sizeof(double),
                          NULL, &err);


    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
        orig_rows * bottom_index * sizeof(double), final_eigen_vectors, 0, NULL, NULL);

    err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
        orig_rows * orig_cols * sizeof(double), image, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
        bottom_index * orig_cols * sizeof(double), temp_output, 0, NULL, NULL);
    err = clblasDgemm(order, clblasTrans, clblasNoTrans, bottom_index, orig_cols, orig_rows,
                      1.0, bufA, 0, bottom_index,
                      bufX, 0, orig_cols,
                      0.0, bufY, 0, orig_cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("Promitnuti clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufY, CL_TRUE, 0, orig_cols * bottom_index * sizeof(double),
                                  temp_output, 0, NULL, NULL);
        checkError(err, "returning result");
    }
    *output = temp_output;
    err = clReleaseMemObject(bufA);
    checkError(err, "Releasing mem bufA");
    err = clReleaseMemObject(bufX);
    checkError(err, "Releasing mem bufX");
    err = clReleaseMemObject(bufY);
    checkError(err, "Releasing mem bufY");
    free(image_mean);
    free(scatter_matrix);
    free(eigen_values);
    free(eigen_vectors);
    free(final_eigen_vectors);
    free(normalized_image);

    /* Release OpenCL memory objects. */
    /* Finalize work with clblas. */
    clblasTeardown();
    /* Release OpenCL working objects. */
    err = clReleaseEvent(event);
    checkError(err, "Releasing event");
    err = clReleaseCommandQueue(queue);
    checkError(err, "Releasing queue");
    err = clReleaseContext(ctx);
    checkError(err, "Releasing context");

    return 0;
}


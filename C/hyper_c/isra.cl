__kernel void isra(
   __global double *image,
   __global double *abundances,
   __global double *endmembers,
   __global double *output,
   const int orig_cols,
   const int orig_rows,
   const int end_cols,
   const int end_rows,
   const int iter) {
    
    double numerator = 0.0;
    double denominator = 0.0;
    double dot = 0.0;
    int idx = get_global_id(0);
    for (int i=0; i<iter; i++) {
        for (int end=0; end<end_cols; end++) {
            for (int band=0; band<end_rows; band++) {

                numerator = numerator + endmembers[band*end_cols+end] * image[band*orig_cols + idx];
                for (int s=0; s<end_cols; s++) {
                    dot += endmembers[band*end_cols+s] * abundances[s*orig_cols + idx];
                }
                denominator += dot*endmembers[band*end_cols+end];
                dot = 0.0;
            }
            abundances[end*orig_cols + idx] = abundances[end*orig_cols + idx] * (numerator / denominator);
            numerator = 0.0;
            denominator = 0.0;
        }
    }
}

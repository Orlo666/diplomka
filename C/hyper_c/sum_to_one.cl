__kernel void sum_to_one(
   __global double *abundances,
   const int abun_rows,
   const int abun_cols) {
    
    int idx = get_global_id(0);
    double sum = 0.0;
    for (int i=0; i<abun_rows; i++) {
        sum += abundances[abun_cols*i + idx];
    }

    for (int i=0; i<abun_rows; i++) {
        abundances[abun_cols*i + idx] = (float)abundances[abun_cols*i + idx]/sum;
    }
}

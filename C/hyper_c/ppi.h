#ifndef PPI_H
#define PPI_H

#ifndef __MULT_HDR
#define __MULT_HDR

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <string.h>
#include <clBLAS.h>
#include <time.h>

#include <err_code.h>

/*
 *
 */
int ppi(double *out_matrix, double *matrix, int orig_rows, int orig_cols, int q, int iter);

#endif
#endif

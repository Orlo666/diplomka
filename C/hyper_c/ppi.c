#include <ppi.h>

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

extern int output_device_info(cl_device_id );
extern char *getKernelSource(char *filename);
extern double wtime();


int ppi(double *out_matrix, double *matrix, int orig_rows, int orig_cols, int q, int iter) {

    double start_time;
    double run_time;
    start_time = wtime();
    double* skewers = malloc(orig_rows * iter * sizeof(double));
    double* copy_matrix = malloc(orig_rows * orig_cols * sizeof(double));
    int* votes = malloc(sizeof(int) * orig_cols);
    //char *output_file = "./neco.mat";
    //save_matrix_to_file(matrix, orig_rows, orig_cols, output_file, "neco");

    memcpy(copy_matrix, matrix, orig_rows * orig_cols * sizeof(double));

    for (int i=0; i<orig_cols; i++) {
        votes[i] = 0;
    }
    char *max_indexes_kernel = getKernelSource("../../hyper_c/max_indexes.cl");

    int *end_members_indexes = malloc(sizeof(int) * q);
    int index = 0;
    // clBLAS init
    clblasOrder order = clblasRowMajor;

    // init OPENCL
    cl_context ctx;       // compute context
    cl_command_queue queue;      // compute command queue
    cl_program program;       // compute program
    cl_kernel kernel;
    cl_device_id device_id;
    cl_context_properties props[3] = { CL_CONTEXT_PLATFORM, 0, 0 };

    cl_mem bufA, bufX, bufY, bufD;
    int err;
    cl_event event = NULL;
    int ret = 0;

    // Fill vectors a and b with random float values

    cl_uint numPlatforms;

    // Find number of platforms
    //
    //

    cl_platform_id platform = 0;

    err = clGetPlatformIDs(1, &platform, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetPlatformIDs() failed with %d\n", err );
        return 1;
    }
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    if (err != CL_SUCCESS) {
        printf( "clGetDeviceIDs() failed with %d\n", err );
        return 1;
    }
    props[1] = (cl_context_properties)platform;


    ctx = clCreateContext(props, 1, &device_id, NULL, NULL, &err);


    // Create a command queue
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating command queue");
    run_time = wtime() - start_time;
    puts("INIT POINTERS and INIT OPENCL");
    printf("%f s\n", run_time);

    start_time = wtime();
    /***************************************************
    * generating random skewers
    ***************************************************/
    double random_number = 0.0;
    srand(time(NULL));
    for (int i=0; i<orig_rows * iter; i++) {
        random_number = (double)rand() / (double)rand();
        skewers[ i] = random_number;
    }

    run_time = wtime() - start_time;
    puts("GENERATING RANDOM SKEWERS:");
    printf("%f s\n", run_time);


    puts("here");
    /**************************************
    * clBLAS
    *************************************/
    // clBLAS Setup
    err = clblasSetup();
    if (err != CL_SUCCESS) {
        printf("clblasSetup() failed with %d\n", err);
        clReleaseCommandQueue(queue);
        clReleaseContext(ctx);
        return 1;
    }


    double *result = malloc(sizeof(double) * orig_cols * iter);

    start_time = wtime();

    bufA = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * orig_cols * sizeof(double),
                          NULL, &err);
    bufX = clCreateBuffer(ctx, CL_MEM_READ_ONLY, orig_rows * iter * sizeof(double),
                          NULL, &err);
    bufY = clCreateBuffer(ctx, CL_MEM_READ_WRITE, orig_cols * iter * sizeof(double),
                          NULL, &err);

    err = clEnqueueWriteBuffer(queue, bufA, CL_TRUE, 0,
        orig_rows * orig_cols * sizeof(double), copy_matrix, 0, NULL, NULL);

    err = clEnqueueWriteBuffer(queue, bufX, CL_TRUE, 0,
        orig_rows * iter * sizeof(double), skewers, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(queue, bufY, CL_TRUE, 0,
        orig_cols * iter * sizeof(double), result, 0, NULL, NULL);
    err = clblasDgemm(order, clblasTrans, clblasNoTrans, orig_cols, iter, orig_rows,
                      1.0, bufA, 0, orig_cols,
                      bufX, 0, iter,
                      0.0, bufY, 0, iter, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    } else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        err = clEnqueueReadBuffer(queue, bufY, CL_TRUE, 0, orig_cols * iter * sizeof(double),
                                  result, 0, NULL, NULL);
        checkError(err, "returning result");

    }
    run_time = wtime() -  start_time;
    printf("MATRIX MULTIPLICATION WITH SKEWERS...\n");
    printf("%f s\n", run_time);


    clReleaseMemObject(bufA);
    clReleaseMemObject(bufX);
    clReleaseMemObject(bufY);
    clReleaseEvent(event);

    clReleaseCommandQueue(queue);

/***************************************************
 * GET MAX IDX OF SKEWERS
 ***************************************************/
    start_time = wtime();


    int * max_idx_array = malloc(iter * sizeof(int));
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    program = clCreateProgramWithSource(ctx, 1, (const char **) & max_indexes_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "max_indexes", &err);
    checkError(err, "Creating kernel");

    bufD = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, orig_cols * iter * sizeof(double),
                          result, &err);
    checkError(err, "Creating buffer bufD");

    bufX = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY, iter * sizeof(int), max_idx_array, &err);
    checkError(err, "Creating buffer bufX");

    size_t global = iter;
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &bufD);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 1, sizeof(int), &orig_cols);
    checkError(err, "Setting kernel arguments");
err |= clSetKernelArg(kernel, 2, sizeof(int), &iter);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &bufX);
    checkError(err, "Setting kernel arguments");
    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, NULL,
        &global, NULL,
        0, NULL, NULL);

    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clEnqueueReadBuffer(queue, bufX, CL_TRUE, 0, iter*sizeof(int),
                              max_idx_array, 0, NULL, NULL);
    checkError(err, "Getting max_idx back from kernel");

    for (int i=0; i<iter; i++) {
        votes[max_idx_array[i]]++;
    }

    run_time = wtime() -  start_time;
    printf("MAX IDX OF SKEWERS...\n");
    printf("%f s\n", run_time);


    /***************************************************
     * COMPUTING VOTES AND ENDMEMBERS
     ***************************************************/
    start_time = wtime();
    err = clReleaseMemObject(bufD);
    checkError(err, "Releasing bufD");
    err = clReleaseKernel(kernel);
    checkError(err, "Releasing kernel");

    err = clReleaseMemObject(bufX);
    checkError(err, "Releasing bufX");
    /* Release OpenCL events. */
    /* Release OpenCL memory objects. */
    /* Finalize work with clblas. */
    clblasTeardown();
    err = clReleaseCommandQueue(queue);
    checkError(err, "Releasing queue");
    err = clReleaseContext(ctx);
    checkError(err, "Releasing context");
    err = clReleaseProgram(program);
    checkError(err, "Releasing Program");

    start_time = wtime();

    int max = 0;
    int max_index = 0;
    for (int i=0; i<q; i++) {
        max = 0;
        max_index = 0;
        for (int j=0; j<orig_cols; j++) {
            if (max < votes[j]) {
                max = votes[j];
                max_index = j;
            }
        }
        votes[max_index] = 0;
        end_members_indexes[i] = max_index;
    }



    for (int i=0; i<orig_rows; i++){
        for (int j=0; j<q; j++) {
            out_matrix[i*q + j] = matrix[i*orig_cols + end_members_indexes[j]];
        }
    }

    run_time = wtime() - start_time;
    printf("FINDING MOST SIGNIFICANT INDICES...\n");
    printf("%f s\n", run_time);
    free(copy_matrix);
    free(skewers);
    free(end_members_indexes);
    free(result);
    free(max_idx_array);
    free(votes);
    free(max_indexes_kernel);


    //free(votes);
    return 0;
}

int ppi_open_cl() {

}

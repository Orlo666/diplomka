#include "eigen.h"

extern int create_identity_matrix(double *matrix, int cols, int rows);
int create_array_of_identity_matrices(double *array, int num_matrices, int cols, int rows);
int matrix_mul(double *x, int x_cols, int x_rows, double *y, int y_cols, int y_rows, double *res);
void matrix_transpose(double *matrix, int cols, int rows);
int qr_decomposition(double *matrix, int cols, int rows, double *matrix_q, double *matrix_r);



int qr_algorithm(double *matrix, int cols, int rows, double *eigen_values, double *eigen_vectors) {
    bool not_convergence = true;
    memcpy(eigen_values, matrix, sizeof(double) * cols * rows);

    // qr decomposition
    double *matrix_q = malloc(sizeof(double) * rows * cols);
    double *matrix_r = malloc(sizeof(double) * rows * cols);

    // q transposed
    double *matrix_q_t = malloc(sizeof(double) * rows * cols);

    double *tmp_matrix = malloc(sizeof(double) * rows * cols);
    double *tmp_matrix_q = malloc(sizeof(double) * rows  * cols);


    int count = 0;
    while(not_convergence) {
        qr_decomposition(eigen_values, cols, rows, matrix_q, matrix_r);
        //puts("AAAAH");
        //for (int i = 0; i<rows; i++) {
        //    puts("");
        //    for(int j = 0; j<rows; j++) {
        //        printf("%f |", eigen_values[i*rows + j]);
        //    }
        //}
        //puts("");
        memcpy(matrix_q_t, matrix_q, sizeof(double) * rows * cols);
        matrix_transpose(matrix_q_t, cols, rows);
        matrix_mul(matrix_q_t, cols, rows, eigen_values, cols, rows, tmp_matrix);
        matrix_mul(tmp_matrix, cols, rows, matrix_q, cols, rows, eigen_values);

        memcpy(tmp_matrix_q, eigen_vectors, sizeof(double) * rows * cols);
        matrix_mul(tmp_matrix_q, cols, rows, matrix_q, cols, rows, eigen_vectors);

        not_convergence = false;
        for (int j = 0; j<cols; j++) {
            for (int k=0; k<rows; k++ ) {
                bool eh = j==k;
                if (j != k &&
                    (eigen_values[j*cols + k] < -0.00000001 ||
                    eigen_values[j*cols + k] > 0.0000001)) {
                    not_convergence = true;
                    break;
                }
            }
            if (not_convergence) {
                break;
            }
        }
        if (count > 100) {
            break;
        }
        printf("%d \n", count);
        count ++;


    }
        puts("AAAAH");
        for (int i = 0; i<rows; i++) {
            puts("");
            for(int j = 0; j<rows; j++) {
                printf("%f |", eigen_values[i*rows + j]);
            }
        }
        puts("");
    free(matrix_q);
    free(matrix_r);
    free(matrix_q_t);
    free(tmp_matrix);
    free(tmp_matrix_q);

}

int create_array_of_identity_matrices(double *array, int num_matrices, int cols, int rows) {
    int matrix_size = cols * rows;
    for (int i=0; i<num_matrices; i++) {
        if (cols != rows) {
            return 1;
        }
        for (int j=0; j<cols*rows; j++) {
            if (j % (cols+1) == 0) {
                array[i*matrix_size + j] = 1.0;
            } else {
                array[i*matrix_size + j] = 0.0;
            }
        }
    }
    return 0;
}


int matrix_mul(double *x, int x_cols, int x_rows, double *y, int y_cols, int y_rows, double *res) {
	for (int i = 0; i < x_rows; i++) {
		for (int j = 0; j < y_cols; j++) {
            res[i*x_cols + j] = 0.0;
			for (int k = 0; k < x_cols; k++) {
				res[i*x_cols + j] += x[i *x_cols + k] * y[k * y_cols + j];
            }
        }
    }

}



void matrix_transpose(double *matrix, int cols, int rows) {
	for (int i = 0; i < rows; i++) {
		for (int j = i; j < cols; j++) {
            if (i !=j) {
                double t = matrix[i *cols + j];
                matrix[i * cols + j] = matrix[j * cols + i];
                matrix[j * cols + i] = t;
            }
		}
	}
}

int qr_decomposition(double *matrix, int cols, int rows, double *matrix_q, double *matrix_r) {
    double *matrix_g = malloc(sizeof(double) * cols * cols);
    double *matrix_g_tmp = malloc(sizeof(double) * cols * cols);
    double *helper = malloc(sizeof(double) * cols * rows);
    double *helper_q = malloc(sizeof(double) * cols * rows);
    //int num_matrices = 1000;
    //double *g_matrices = malloc(sizeof(double) * cols * cols * num_matrices);

    create_identity_matrix(matrix_g, cols, rows);
    memcpy(matrix_r, matrix, sizeof(double) * cols * cols);

    //create_array_of_identity_matrices(g_matrices, num_matrices, cols, rows);

    double c = 0.0;
    double s = 0.0;
    double r = 0.0;
    double x1 = 0.0;
    double x2 = 0.0;
    double ik = 0.0;
    double jk = 0.0;

    int n = 0;
    printf("cols %d\n", rows);
    for (int i=0; i<cols-1; i++) {
        for (int j=i+1; j<rows ; j++) {
            //int j = i+1;
            x1 = matrix_r[i*cols + i];
            x2 = matrix_r[j*cols + i];
            //if (i == 0 && j==rows-1) {
            //    continue;
            //}
            if (x2 != 0.0) {
                memcpy(matrix_g_tmp, matrix_g, sizeof(double)*cols*rows);
                create_identity_matrix(matrix_g, cols, rows);

                //puts(" *********** MATRIX G ******************");

                //for (int k=0; k<cols; k++) {
                //    puts("");
                //    for (int s=0; s<rows; s++) {
                //        if (s == rows-1) {
                //            printf("%f ; ", matrix_g[k*cols + s]);
                //        } else {
                //            printf("%f , ", matrix_g[k*cols + s]);
                //        }

                //    }
                //};
                //puts("\n\n");

                r = sqrt(pow(x1, 2) + pow(x2, 2));
                if (r < 0.0) {
                    r = abs(r);
                }
                c = (double) (x1/r);
                s = (double) (x2/r);

                matrix_g[i*cols + i] = c;
                matrix_g[i*cols + j] = s;
                matrix_g[j*cols + i] = -s;
                matrix_g[j*cols + j] = c;

                // double *g_second = &g_matrices[n*cols*rows];



                for (int i=0; i<rows*cols; i++) {
                    helper[i] = 0.0;
                }

                //puts(" *********** MATRIX G ******************");

                //for (int k=0; k<cols; k++) {
                //    puts("");
                //    for (int s=0; s<rows; s++) {
                //        if (s == rows-1) {
                //            printf("%f ; ", matrix_g[k*cols + s]);
                //        } else {
                //            printf("%f , ", matrix_g[k*cols + s]);
                //        }

                //    }
                //};
                //puts("\n\n");

                puts("f");
                matrix_mul(matrix_g, cols, rows, matrix_r, cols, rows, helper);
                puts("a");
                memcpy(matrix_r, helper, sizeof(double) * rows *cols);
                //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                //            3, 3, 3, 1., &g_second, 3, matrix_r,
                //            3, 1., matrix_r, 3);
                //for (int i=0; i<rows*cols; i++) {
                //    matrix_r[i] = (double)matrix_r[i] - helper[i];
                //}
                if (n == 0) {
                    matrix_transpose(matrix_g, rows, cols);
                    memcpy(matrix_q, matrix_g, sizeof(double) * rows * cols);
                }
                if (n != 0) {
                    matrix_transpose(matrix_g, rows, cols);
                    matrix_mul(matrix_q, cols, rows, matrix_g, cols, rows, helper_q);
                    memcpy(matrix_q, helper_q, sizeof(double) * rows *cols);
                }
                n++;
                /* Application of G on A on fly
                for (int k=i; k<cols; k++) {
                    ik = matrix[i*cols+k];
                    jk = matrix[j*cols+k];
                    printf("jk %f\n", jk);
                    printf("ik %f\n", ik);
                    matrix[i*cols+k] = ik * c + jk * s;
                    matrix[j*cols+k] = jk * c + (ik * -1 * s);
                }
                */

                /* Storing s and c in same array as A
                if (c > -0.00000001 && c < 0.0000001) {
                    printf("ne\n");
                    matrix[j*cols + i] = 1.0;
                } else if (abs(s) < abs(c)) {
                    printf("uf\n");
                    if (c < 0.0) {
                        puts("Eeee");
                        matrix[j*cols + i] = -0.5 * s;
                    }
                    else {
                        puts("dfadfa");
                        matrix[j*cols + i] = 0.5 * s;
                    }
                }
                else {
                    puts("erer");
                    printf("%f \n",matrix[j * cols + i]);
                    matrix[j * cols + i] = (2.0 / c);
                    printf("%f \n",matrix[j * cols + i]);
                }
                */
            }
        }
    }


    //double *g_first = &g_matrices[0];
    //matrix_transpose(g_first, rows, cols);
    //memcpy(matrix_q, g_first, sizeof(double) * rows * cols);


    //for (int j=1; j<n; j++) {
    //    double *g_second = &g_matrices[j*cols*rows];

    //    //cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
    //    //            rows, rows, rows, 1., matrix_q, cols, g_second, cols, 1., matrix_q, cols);
    //    for (int i=0; i<rows*cols; i++) {
    //        helper[i] = 0.0;
    //    }
    //    matrix_transpose(g_second, cols, rows);
    //    matrix_mul(matrix_q, cols, rows, g_second, cols, rows, helper);
    //    memcpy(matrix_q, helper, sizeof(double) * rows *cols);
    //    //for (int i=0; i<rows*cols; i++) {
    //    //    matrix_r[i] = (double)matrix_r[i] - helper[i];
    //    //}

    //}
    //puts("AAAAAAAAA\n");
    //free(g_matrices);


    double *matrix_ll = malloc(sizeof(double) * rows * cols);

    //  cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
    //              rows, rows, rows, 1., matrix_q, cols, matrix_r, cols, 0., matrix_ll, cols);
    //for (int i=0; i<cols*rows; i++) {
    //    matrix_ll[i] = 0.0;
    //}
    //matrix_mul(matrix_q, cols, rows, matrix_r, cols, rows, matrix_ll);
    free(matrix_ll);
    free(matrix_g);
    free(matrix_g_tmp);
    free(helper);
    free(helper_q);
}


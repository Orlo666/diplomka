#ifndef FCLS_H
#define FCLS_H
#ifndef __MULT_HDR
#define __MULT_HDR

#include <mat.h>
#include <math.h>
#include <mex.h>
#include <string.h>
#include <clBLAS.h>
#include <time.h>
#include <lapacke.h>

#include "err_code.h"
#include "utils.h"


/*
 * E: endmembers
 * (E'*E)*E'
 */
int non_constrained(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols);

/*
 * E: endmembers
 * E'*E
 */
int non_constrained_simple(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols);

/*
 * E: endmembers
 * x: original vector of img
 * PHI^(k+1) = PHI^k * ((E'*x) / (E'*E*PHI^k))
 */
int isra(double *abundances, double *input_matrix, int orig_rows, int orig_cols, double * endmembers, int end_rows, int end_cols);

/*
 */
int sum_to_one(double *abundances, int abun_cols, int abun_rows);

#endif
#endif

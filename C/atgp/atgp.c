#include <mat.h>
#include <math.h>
#include <mex.h>
#include <gsl/gsl_sort.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_sort.h"
#include "gsl/gsl_sort_vector.h"
#include "../matlab_io.h"
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>
#include <sys/time.h>

int main() {
    char* end_members_file = "../../data/Data_MAT/end3.mat";
    char* original_file = "../../data/Data_MAT/samson_1.mat";
    char* covariance_file = "../../data/Data_MAT/covariance.mat";
    char* output_file = "../../data/Data_MAT/C_endmembers_atgp.mat";

    mxArray *orig_pa = NULL;
    gsl_matrix *orig_matrix;
    gsl_matrix *orig_backup;
    gsl_matrix *end_members;
    int orig_cols;
    int orig_rows;
    double *orig_data;
    double *end_data;
    int num_endmembers = 3;

    orig_pa = load_variable_from_file(original_file, "V");

    if (orig_pa == NULL) {
        printf("moje chyba v orig_pa\n");
        return 1;
    }

    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_cols = mxGetN(orig_pa);
    orig_matrix= gsl_matrix_alloc (orig_rows, orig_cols);

    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_cols; j++) {
            gsl_matrix_set(orig_matrix, i, j, orig_data[j*orig_rows + i]);
        }
    }

    gsl_vector_view orig_col;
    gsl_vector_view max_orig_col;
    gsl_vector *scaled_vector = gsl_vector_alloc(orig_rows);
    gsl_matrix *final_matrix = gsl_matrix_alloc(orig_rows, num_endmembers);
    double t0 = 0.0;
    double max = -1.0;
    int max_idx = 0;


    for (int i=0; i<orig_cols; i++) {
        orig_col = gsl_matrix_column(orig_matrix, i);
        gsl_blas_ddot(&orig_col.vector, &orig_col.vector, &t0);
        
        if (max < t0) {
            max = t0;
            max_idx = i;
            max_orig_col = orig_col;
        }
    }
    
    int s = 0;
    gsl_matrix *one_column_matrix = gsl_matrix_alloc(orig_rows, 1);
    gsl_matrix *one_row_matrix = gsl_matrix_alloc(1, orig_rows);
    gsl_matrix *identity_matrix = gsl_matrix_alloc(orig_rows, orig_rows);
    gsl_matrix *projection_matrix = gsl_matrix_alloc(orig_rows, orig_rows);
    gsl_matrix * sub_projection_matrix = gsl_matrix_alloc(orig_rows, orig_rows);
    gsl_matrix *fin_working_matrix = gsl_matrix_alloc(orig_rows, num_endmembers);

    gsl_vector *sub_vector = gsl_vector_alloc(orig_rows);

    for (int iter=0; iter<num_endmembers; iter++) {
        // main loop

        gsl_matrix_set_col(fin_working_matrix, iter, &max_orig_col.vector);
        gsl_matrix_view working_matrix = gsl_matrix_submatrix(fin_working_matrix, 0, 0, orig_rows, iter+1);

        gsl_matrix * tmp_matrix_i_i = gsl_matrix_alloc(iter+1, iter+1);
        gsl_matrix * tmp_matrix_r_i = gsl_matrix_alloc(orig_rows, iter+1);
        gsl_matrix * tmp_matrix_r_r = gsl_matrix_alloc(orig_rows, orig_rows);

        gsl_matrix_set_identity(identity_matrix);
        gsl_permutation * permutation = gsl_permutation_alloc (iter+1);
        gsl_matrix * inverse = gsl_matrix_alloc(iter+1, iter+1);

        float vector_result;
        gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, &working_matrix.matrix, &working_matrix.matrix, 0.0, tmp_matrix_i_i);

        s = 0;
        gsl_linalg_LU_decomp (tmp_matrix_i_i, permutation, &s);
        gsl_linalg_LU_invert (tmp_matrix_i_i, permutation, inverse);

        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, &working_matrix.matrix, inverse, 0.0, tmp_matrix_r_i);
        gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, tmp_matrix_r_i, &working_matrix.matrix, 0.0, tmp_matrix_r_r);


        gsl_matrix_memcpy(sub_projection_matrix, identity_matrix);
        gsl_matrix_sub(sub_projection_matrix, tmp_matrix_r_r);

        t0 = 0.0;
        max = -1.0;
        max_idx = 0;
        for (int i=0; i<orig_cols; i++) {
            orig_col = gsl_matrix_column(orig_matrix, i);           
            gsl_blas_dgemv(CblasNoTrans, 1.0, sub_projection_matrix, &orig_col.vector, 0.0, sub_vector);
            gsl_blas_ddot(sub_vector, sub_vector, &t0);
            if (max < t0) {
                max = t0;
                max_idx = i;
            }
        }
        max_orig_col = gsl_matrix_column(orig_matrix, max_idx);
        gsl_matrix_free(tmp_matrix_i_i);
        gsl_matrix_free(tmp_matrix_r_i);
        gsl_matrix_free(tmp_matrix_r_r);
        gsl_permutation_free(permutation);
        gsl_matrix_free(inverse);
    }
    //gsl_matrix_set_col(fin_working_matrix, num_endmembers-1, &max_orig_col.vector);
    save_matrix_to_file(fin_working_matrix,output_file, "atgp_c");
}

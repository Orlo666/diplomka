#ifndef __MULT_HDR
#define __MULT_HDR
#include "mat.h"
#include "mex.h"
#include "matrix_lib.h"
#include <string.h>


void jacobi(double *cov, double *eigenValues, double *eigenVectors, int rows, int cols);

double frobeniusNorm(double* matrix, int rows, int cols);

void abs_matrix(double *matrix, int rows, int cols);

void findMax(double *matrix, int rows, int cols, int *max_row, int *max_col);

void zeroRowCol(double *matrix_s, double *matrix_u, int rows, int cols, int iter_row, int iter_col);

void identity_matrix(double *matrix, int n);

int maxInd(double *matrix, int k, int n);

void update(double* vector, bool *changed, int *state, int k, double t);

void rotate(double *matrix, int k, int l, int i, int j, int c, int s, int n);

#endif

//------------------------------------------------------------------------------
//
//  PROGRAM: Matrix library include file (function prototypes)
//
//  HISTORY: Written by Tim Mattson, August 2010 
//           Modified by Simon McIntosh-Smith, September 2011
//           Modified by Tom Deakin and Simon McIntosh-Smith, October 2012
//           Ported by Tom Deakin, July 2013
//
//------------------------------------------------------------------------------

#ifndef __MATRIX_LIB_HDR
#define __MATRIX_LIB_HDR

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


//------------------------------------------------------------------------------
//
//  Function to compute the matrix product (sequential algorithm, dot producdt)
//
//------------------------------------------------------------------------------
void seq_mat_mul_sdot(int N, double *A, double *B, double *C);

//------------------------------------------------------------------------------
//
//  Function to set a matrix to zero 
//
//------------------------------------------------------------------------------
void zero_mat (int N, double *C);

//------------------------------------------------------------------------------
//
//  Function to fill Btrans(Mdim,Pdim)  with transpose of B(Pdim,Mdim)
//
//------------------------------------------------------------------------------
void trans(int N, double *B, double *Btrans);

#endif

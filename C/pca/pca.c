#include "pca.h"
#define EPS 10e-08

void abs_matrix(double *matrix, int rows, int cols) {
    for (int k=0; k<rows; k++) {
        for (int i=0; i<cols; i++) {
            matrix[k*rows + i] = fabs(matrix[k*rows + i]);
        }
    }
}

void findMax(double *matrix, int rows, int cols, int *max_row, int *max_col) {
    double m = matrix[0];
    *max_row = 0;
    *max_col = 0;
    int n = rows;
    for (int k=0; k<rows; k++) {
        for (int l=0; l<rows; l++) {
            if (matrix[rows*k + l] > m) {
                *max_row = k;
                *max_col = l;
                m = matrix[rows*k + l];
            }
        }
    }
}

void zeroRowCol(double *matrix_s, double *matrix_u, int rows, int cols, int iter_row, int iter_col) {
    double t,c,s, theta;
    if (iter_row == iter_col) return;

    theta=(matrix_s[iter_row*rows + iter_row]-matrix_s[iter_col* rows + iter_col])/(2*matrix_s[iter_row*rows + iter_col]);
    if (theta < EPS) {
        t = 1/(fabs(theta) + sqrt(theta+theta+1));
    } else {
        t = 1/fabs(2*theta);
    }

    if (theta < 0) {
        t = -t;
    }

    c = 1/sqrt(t*t + 1);
    s = c*t;
    double *matrix_r = malloc(sizeof(double) * rows * rows);
    identity_matrix((double *)matrix_r, rows);


    matrix_r[iter_row*rows + iter_row] = matrix_r[iter_col * rows + iter_col] = c; 
    matrix_r[iter_row*rows + iter_col] = s;
    matrix_r[iter_col * rows + iter_row] = -s; 

    double *matrix_r_trans = malloc(sizeof(double) * rows * rows);
    trans(rows, matrix_r, matrix_r_trans);
    seq_mat_mul_sdot(rows, matrix_s, matrix_r_trans, matrix_s);
    free (matrix_r);
    matrix_r = matrix_r_trans;

    double *res_matrix = malloc(sizeof(double)*rows*rows);
    seq_mat_mul_sdot(rows, matrix_r, matrix_s, res_matrix);
    memcpy(matrix_s, res_matrix, sizeof(double)*rows*rows);

    seq_mat_mul_sdot(rows, matrix_u, matrix_r, res_matrix);
    memcpy(matrix_u, res_matrix, sizeof(double)*rows*rows);
    free(matrix_r);
}

void identity_matrix(double *matrix, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                matrix[n*i + j] = 1.0;
            } else {
                matrix[n*i + j] = 0.0;
            }
        }
    }
}

double frobenius_norm(double* matrix, int rows, int cols)
{
    double result = 0.0;
    for(int i = 0; i < rows; ++i) {
        for(int j = 0; j < cols; ++j) {
            double value = *(matrix + (i*cols) + j);
            result += value * value;
        }
    }
    return sqrt(result);
}

int maxInd(double *matrix, int k, int n) {
    int m = k+1;
    for (int i = k+2; i < n; i++) {
        if (matrix[k*n + i] >  matrix[k*n + m]) {
            m = i;
        }
    }
    return m;

}

void update(double* vector, bool *changed, int *state, int k, double t) {
    double y = vector[k];
    vector[k] = y + t;
    if (changed[k] && y == vector[k]) {
        changed[k] = false;
        printf("cool\n");
        *state--;
    } else if (!changed[k] && y != vector[k]) {
        changed[k] = true;
        printf("noooope\n");
        *state++;
    } else {
        printf("---------------------------------\n");
    }
}


void rotate(double *matrix, int k, int l, int i, int j, int c, int s, int n) {
    double kl = matrix[k*n + l];
    double ij = matrix[i*n + j];
    matrix[k*n +l] = kl * c + ij * (-1 * s);
    matrix[i*n + l] = kl * s + ij * c;
}




void jacobi(double *cov, double *eigenValues, double *eigenVector, int rows, int cols) {
/*    int iter_col, iter_row;
    bool iterating = true;
    if (rows != cols) {
        exit(-1);
    }
    double *matrix_m;
    matrix_m = malloc(sizeof(double)*rows*rows);
    

    identity_matrix(eigenVector, rows);
    printf("cool here\n");
    while(iterating) {
        memcpy(matrix_m, cov, sizeof(double) * rows *rows);
        abs_matrix(matrix_m, rows, cols);

        for (int k=0; k<rows; k++) {
            matrix_m[k*rows + k] = 0.0;
        }

        findMax(matrix_m, rows, cols, &iter_row, &iter_col);
        if (iter_row == iter_col) {
            for (int i=0; i<rows; i++) {
                eigenValues[i] = cov[i*rows + i];
            }
            return;
        }
        double s_max = cov[iter_row * rows + iter_col];
        zeroRowCol (cov, eigenVector, rows, cols, iter_row, iter_col);
        if (s_max < EPS * frobenius_norm(cov, rows, cols)) {
            iterating = false;
        }
    }
    for (int i=0; i<rows; i++) {
        eigenValues[i] = cov[i*rows + i];
    }

    free (matrix_m);*/
    int i, k, l, m, state;
    double s, c, t, p, y, d, r;
    int *ind = malloc(sizeof(int) * rows);
    bool *changed = malloc(sizeof(bool) * rows);

    int n = rows;
    identity_matrix(eigenVector, n);    
    state = n;


    double frob;
    bool iterating = true;
    for (int k = 0; k < n; k++) {
        ind[k] = maxInd(cov, k, n);
        eigenValues[k] = cov[k*n + k];
        changed[k] = true;
    }

    state = 5;
    //while(state != 0) {
    while(state != 0) {
        m = 0;
        for (int k = 1; k < n-1; k++) {
            if (cov[k*n + ind[k]] > cov[m*n + ind[m]]) {
                m = k;
            }
        }
        //printf("\n");
        //state = 1;
        k = m;
        l = ind[m];
        //printf ("l %f and k %d\n", l, k);
        p = cov[k+n + l];
        y = (eigenVector[l] - eigenVector[k]) / 2.0;
        //printf ("p %f and y %f\n", p, y);
        d = fabs(y) + sqrt(pow(p, 2) + pow(y,2));
        r = sqrt(pow(p, 2) + pow(d,2));
        c = d/r;
        s = p/r;
        //printf ("p %f and d %f\n", p,d);
        t = pow(p, 2)/d;
        if (y<0) {
            s = -1 * s;
            t = -1 * t;
        }
        cov[k*n + l] = 0.0;
        //printf("WHAAT %f\n", t);
        update(eigenVector, changed, &state, k, -1*t);
        update(eigenVector, changed, &state, l, t);
        for (int i = 0; i < k-1; i++) {
            rotate(cov, i, k, i, l, c, s, n);
        }
        

        for (int i = k+1; i < l-1; i++) {
            rotate(cov, k, i, i, l, c, s, n);
        }
        for (int i = l+1; i < n; i++) {
            rotate(cov, k, i, l, i, c, s, n);
        }
        for (int i = 0; i < n; i++) {
            rotate(eigenVector, i, k, i, l, c, s, n);
        }

        ind[k] = maxInd(cov, k, n);
        ind[l] = maxInd(cov, l, n);

        state --;
    }

    free (ind);
    free (changed);
}



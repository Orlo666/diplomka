#include "pca.h"
#include "mat.h"
#include "mex.h"

int main() {
    char* end_members_file = "../data/Data_MAT/end3.mat";
    char* original_file = "../data/Data_MAT/samson_1.mat";
    char* covariance_file = "../../data/Data_MAT/covariance.mat";



    MATFile *pmat_file;
    const char **dir;
    const char *name;
    int	  ndir;
    int	  i;
    mxArray *pa;
    double *eigenValues;
    double *eigenVectors;

    int bands_num;
    int pixel_num;
   
    printf("Reading file %s...\n\n", covariance_file);

    /*
     * Open file to get directory
     */
    pmat_file = matOpen(covariance_file, "r");
    if (pmat_file == NULL) {
        printf("Error opening file %s\n", covariance_file);
        return(1);
    }
    
    /*
     * get directory of MAT-file
     */
    dir = (const char **)matGetDir(pmat_file, &ndir);
    if (dir == NULL) {
      printf("Error reading directory of file %s\n", covariance_file);
      return(1);
    } else {
      printf("Directory of %s:\n", covariance_file);
      for (i=0; i < ndir; i++)
        printf("%s\n",dir[i]);
    }
    mxFree(dir);

    /* In order to use matGetNextXXX correctly, reopen file to read in headers. */
    if (matClose(pmat_file) != 0) {
      printf("Error closing file %s\n", covariance_file);
      return(1);
    }
    pmat_file = matOpen(covariance_file, "r");
    if (pmat_file == NULL) {
      printf("Error reopening file %s\n", covariance_file);
      return(1);
    }
    pa = matGetVariable(pmat_file, "Covariance");
    if (pa == NULL) {
        printf("Cannot read Covariance\n");
        return 1;
    }

    bands_num = mxGetM(pa);

    printf("tu okej setko\n");
    
    double *variance_pointer = mxGetPr(pa);
    printf("%f\n", variance_pointer[0]);

    printf("tu okej setko\n");


    eigenVectors = malloc(sizeof(double) * bands_num *bands_num);
    if (eigenVectors == NULL) {
        printf("EigenValue not enough space\n");
        return 1;
    }
    eigenValues = malloc(sizeof(double) * mxGetM(pa));
    if (eigenValues == NULL) {
        printf("eigenValue malloc failed, not enought space");
        return 1;
    }
    pixel_num = mxGetN(pa);

    jacobi(variance_pointer, eigenValues, eigenVectors, bands_num, pixel_num);

    free(eigenValues);
    /*
    printf("pohodicka\n");
    for (int x=0; x<bands_num; x++) {
        printf("%f ",eigenValues[x]);
    }

    for (int x=0; x<bands_num; x++) {
        printf("%f\n",eigenVectors[x*bands_num + x]);
    }
    */
    free(eigenVectors);
}

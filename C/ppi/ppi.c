#include <mat.h>
#include <math.h>
#include <mex.h>
#include <gsl/gsl_sort.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_vector.h"
#include "gsl/gsl_sort.h"
#include "gsl/gsl_sort_vector.h"
#include "../matlab_io.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>
#include <sys/time.h>

extern double wtime();

void ppi(gsl_matrix **out_matrix, gsl_matrix *matrix, int q, int iter) {
    double run_time;
    double start_time;
    int orig_rows = matrix->size1;
    int orig_columns = matrix->size2;

    gsl_matrix* copy_matrix = gsl_matrix_alloc(orig_rows, orig_columns);
    gsl_matrix* skewers = gsl_matrix_alloc(orig_rows, iter);

    
    double* votes = malloc(sizeof(double) * orig_columns);
    gsl_matrix_memcpy(copy_matrix, matrix);


    // removing mean from data

    double mean = 0.0;
    for (int i=0; i<orig_rows; i++) {
        for (int j=0; j<orig_columns; j++) {
            mean += gsl_matrix_get(copy_matrix, i, j);
        }
        mean /= orig_columns;
        for (int j=0; j<orig_columns; j++) {
            gsl_matrix_set(copy_matrix, i, j, gsl_matrix_get(copy_matrix, i, j) - mean);
        }
        mean = 0.0;
    }

    const gsl_rng_type * T;
    gsl_rng * r;
    double mu = 1;

    gsl_rng_env_setup();
    struct timeval tv; // Seed generation based on time
    gettimeofday(&tv,0);
    unsigned long mySeed = tv.tv_sec + tv.tv_usec;
    T = gsl_rng_default; // Generator setup
    r = gsl_rng_alloc (T);
    gsl_rng_set(r, mySeed);

    for (int i=0; i<orig_rows; i++) {
        for (int j=0; j<iter; j++) {
            double k = gsl_ran_gaussian (r, mu);
            gsl_matrix_set(skewers, i, j, k);
        }
    }

    gsl_rng_free (r);

    gsl_vector_view vector_view;
    gsl_vector *vector;
    vector = gsl_vector_calloc(orig_columns);
    int *end_members_indexes = malloc(sizeof(int) * q*2);
    int index = 0;
    for (int i=0; i<orig_columns; i++) {
        votes[i] = 0.0;
    }

    start_time = wtime();
    for (int i=0; i<iter; i++) {
        vector_view = gsl_matrix_column(skewers, i);

        gsl_blas_dgemv (CblasTrans, 1.0, copy_matrix, &vector_view.vector, 0.0, vector);

        for(int j=0; j<orig_columns; j++) {
            gsl_vector_set(vector, j, fabs(gsl_vector_get(vector, j)));
        }

        index = gsl_vector_max_index(vector);

        votes[index]+= 1.0;

    }
    run_time = wtime() - start_time;
    printf("%d iteration of matrix mult...\n", iter);
    printf("%f s\n", run_time);

    start_time = wtime();


    int one = 0;
    int two = 0;
    int three = 0;
    double max = 0.0;

    for (int i=0; i<orig_columns; i++) {
        if (max < votes[i]) {
            max = votes[i];
            one = i;
        }
    }
    votes[one] = 0;
    max = 0;
    for (int i=0; i<orig_columns; i++) {
        if (max < votes[i]) {
            max = votes[i];
            two = i;
        }
    }
    votes[two] = 0;
    max = 0;
    for (int i=0; i<orig_columns; i++) {
        if (max < votes[i]) {
            max = votes[i];
            three = i;
        }
    }
    run_time = wtime() - start_time;
    printf("%d iteration of matrix mult...\n", iter);
    printf("%f s\n", run_time);

    printf("o %d t %d f %d ", one, two, three);
    end_members_indexes[0] = one;
    end_members_indexes[1] = two;
    end_members_indexes[2] = three;



    for (int i=0; i<orig_rows; i++){
        for (int j=0; j<q; j++) {
            gsl_matrix_set(*out_matrix, i, j, gsl_matrix_get(matrix, i, end_members_indexes[j]));
        }
    }

    gsl_matrix_free(copy_matrix);
    gsl_matrix_free(skewers);
    free(votes);
    free(end_members_indexes);
    return;

    
}

int main () {
    char* end_members_file = "../../data/Data_MAT/end3.mat";
    char* original_file = "../../data/Data_MAT/samson_1.mat";
    char* covariance_file = "../../data/Data_MAT/covariance.mat";
    char* output_file = "../../data/Data_MAT/C_ppi.mat";

    mxArray *orig_pa = NULL;
    gsl_matrix *orig_matrix;
    gsl_matrix *output;
    int orig_columns;
    int orig_rows;
    int q = 3;
    int iter = 1000;
    double *orig_data;

    orig_pa = load_variable_from_file(original_file, "V");

    if (orig_pa == NULL) {
        printf("moje chyba v orig_pa\n");
        return 1;
    }

    orig_data = mxGetPr(orig_pa);
    orig_rows = mxGetM(orig_pa);
    orig_columns = mxGetN(orig_pa);
    orig_matrix= gsl_matrix_alloc (orig_rows, orig_columns);

    for (int i = 0; i < orig_rows; i++) {
        for (int j = 0; j < orig_columns; j++) {
            gsl_matrix_set(orig_matrix, i, j, orig_data[j*orig_rows + i]);
        }
    }


    output = gsl_matrix_alloc(orig_rows, q);
    ppi(&output, orig_matrix, q, iter);

    save_matrix_to_file(output, output_file, "ppi_c");

    gsl_matrix_free(orig_matrix);
    gsl_matrix_free(output);
}



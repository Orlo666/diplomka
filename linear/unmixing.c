//  PROGRAM: Matrix Multiplication driver
//
//  PURPOSE: This is a driver program to test various ways of computing
//           the product:
//
//                C  = A * B
//
//           A and B are set to constant matrices so we
//           can make a quick test of the multiplication.
//
//  USAGE:   The matrices are constant matrices, square and the order is
//           set as a constant, ORDER (see mult.h).
//
//  HISTORY: Written by Tim Mattson, August 2010
//           Modified by Simon McIntosh-Smith, September 2011
//           Modified by Tom Deakin and Simon McIntosh-Smith, October 2012
//           Ported to C by Tom Deakin, July 2013
//           Updated to assume square matricies by Tom Deakin and
//           Simon McIntosh-Smith, October 2014
//
//------------------------------------------------------------------------------

#include "unmixing.h"
#include "err_code.h"
#include "device_picker.h"
#include "mat.h"
#include "mex.h"
#include "string.h"

char * getKernelSource(char *filename);

int main(int argc, char *argv[])
{
    float *h_A;             // A matrix
    float *h_B;             // B matrix
    float *h_C;             // C = A*B matrix
    int N;                  // A[N][N], B[N][N], C[N][N]
    int size;               // number of elements in each matrix

    cl_mem d_a, d_b, d_c;   // Matrices in device memory

    double start_time;      // Starting time
    double run_time;        // timing data

    char * kernelsource;    // kernel source string

    cl_int err;             // error code returned from OpenCL calls
    cl_device_id     device;        // compute device id
    cl_context       context;       // compute context
    cl_command_queue commands;      // compute command queue
    cl_program       program;       // compute program
    cl_kernel        kernel;        // compute kernel

    N = ORDER;


    size = N * N;

    char* end_members_file = "../data/Data_MAT/end3.mat";
    char* original_file = "../data/Data_MAT/samson_1.mat";



    MATFile *pmat;
    const char **dir;
    const char *name;
    int	  ndir;
    int	  i;
    mxArray *pa;
   
    printf("Reading file %s...\n\n", end_members_file);

    /*
     * Open file to get directory
     */
    pmat = matOpen(end_members_file, "r");
    if (pmat == NULL) {
        printf("Error opening file %s\n", end_members_file);
        return(1);
    }
    
    /*
     * get directory of MAT-file
     */
    dir = (const char **)matGetDir(pmat, &ndir);
    if (dir == NULL) {
      printf("Error reading directory of file %s\n", end_members_file);
      return(1);
    } else {
      printf("Directory of %s:\n", end_members_file);
      for (i=0; i < ndir; i++)
        printf("%s\n",dir[i]);
    }
    mxFree(dir);

    /* In order to use matGetNextXXX correctly, reopen file to read in headers. */
    if (matClose(pmat) != 0) {
      printf("Error closing file %s\n",end_members_file);
      return(1);
    }
    pmat = matOpen(end_members_file, "r");
    if (pmat == NULL) {
      printf("Error reopening file %s\n", end_members_file);
      return(1);
    }

    /* Read in each array. */
    printf("\nReading in the actual array contents:\n");
    double *m_values = NULL;
    int m_rows = 0;
    int m_cols = 0;
    size_t num_elements = 0;
    for (i=0; i<ndir; i++) {
        pa = matGetNextVariable(pmat, &name);
        printf("------------------------------------------------\n");
        printf("rows %d \n", mxGetM(pa));
        printf("cols %d \n", mxGetN(pa));
        if (pa == NULL) {
            printf("Error reading in file %s\n", end_members_file);
            return(1);
        } 
        /*
         * Diagnose array pa
         */
        if (strncmp(name, "M", 1) == 0) {
            m_rows = mxGetM(pa); 
            m_cols = mxGetN(pa);
            num_elements = mxGetNumberOfElements(pa);
            m_values = (double *)calloc(sizeof(double), num_elements);
            memcpy(m_values, mxGetData(pa), num_elements*sizeof(double));
        }
        printf("According to its contents, array %s has %ld dimensions\n",
           name, mxGetNumberOfDimensions(pa));
        if (mxIsFromGlobalWS(pa)) {
            printf("  and was a global variable when saved\n");
        } else {
            printf("  and was a local variable when saved\n");
            mxDestroyArray(pa);
        }
    }

    if (matClose(pmat) != 0) {
        printf("Error closing file %s\n",end_members_file);
        return(1);
    }
    printf("Done\n");
    printf("data: %f\n", m_values[0]);


    // original image
    pmat = matOpen(original_file, "r");
    if (pmat == NULL) {
        printf("Error opening file %s\n", original_file);
        return(1);
    }
    
    pa = matGetVariable(pmat, "V");
    if (pa == NULL) {
        printf("getting the variable V failed\n");
        return 1;
    }

    num_elements = mxGetNumberOfElements(pa);

    double *orig_values = (double *)calloc(sizeof(double), num_elements);


    int orig_rows = mxGetM(pa);
    int orig_cols = mxGetN(pa);
    double *image_values = mxGetData(pa);
    
    if (image_values == NULL) {
        printf("not happening\n");
        return 1;
    }
    double *r = (double *)calloc(sizeof(double), orig_cols);

    for (int c = 0; c < orig_rows; c++) {
        r[c] = image_values[c];
    }


    if (mxIsFromGlobalWS(pa)) {
        mxDestroyArray(pa);
    }
    if (matClose(pmat) != 0) {
        printf("Error closing file %s\n",original_file);
        return(1);
    }

    // Non negativity constraint NNCLS
    double numerator = 0;
    double Fi[3] ={
        -0.0101300305204599,
        0.00487137420436867,
        0.0761668656891977};
    Fi_new[3] = {0.0, 0.0, 0.0};
    double denominator = 0;
    int iters = 1;
    double dot = 0;
    for (int k = 0; k<iters; k++) {
        for (int j=0; j<m_cols; j++) {
            for (int i=0; i<m_rows; i++) {
                numerator += m_values[i+j*m_rows] * r[i];
                if (i % 10 == 0) {
                    printf("num %f ri %f\n", numerator, r[i]);
                }
                for (int s=0; s<m_cols; s++) {
                    dot += m_values[i+s*m_rows] * Fi[s];
                    if (i % 10 == 0) {
                        
                        printf("dot %f\n", dot);
                    }

                }
                denominator += dot * m_values[i+j*m_rows];
                if (i % 10 == 0) {
                    printf("DENOM %f\n", denominator);
                }
                dot = 0;
            }
            printf("numberator %f / denominator %f in %i step\n", numerator, denominator, j);
            Fi[j] = Fi[j] * (numerator / denominator);
            numerator = 0;
            denominator = 0;
        }
    }
    for (int i; i<m_cols; i++) {
        printf("%f\n", Fi[i]);
    }
    if (m_values) {
        free(m_values);
    }
    free(orig_values);


    return EXIT_SUCCESS;
}


char * getKernelSource(char *filename)
{
    FILE *file = fopen(filename, "r");
    if (!file)
    {
        fprintf(stderr, "Error: Could not open kernel source file\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    int len = ftell(file) + 1;
    rewind(file);

    char *source = (char *)calloc(sizeof(char), len);
    if (!source)
    {
        fprintf(stderr, "Error: Could not allocate memory for source string\n");
        exit(EXIT_FAILURE);
    }
    int a = fread(source, sizeof(char), len, file);

    fclose(file);
    return source;
}

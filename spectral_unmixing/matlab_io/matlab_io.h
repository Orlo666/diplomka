#ifndef MATLABIO_H
#define MATLABIO_H
#ifndef __MULT_HDR
#define __MULT_HDR


#include "mat.h"
#include "mex.h"
#include <clBLAS.h>

#include <gsl/gsl_matrix.h>

/**
 * Loading variable from matlab file
 *
 * @param file_name: path to the matlab file
 * @param variable_name: variable we want to load
 *
 * @return: Matrix in mxArray format
 */
mxArray* load_variable_from_file(char *file_name, char *variable_name);


/**
 * Loading variable from mxArray
 *
 * @param memory: pointer to pointer represents original image in memory
 * @param mat: mxArray loaded from matlab file
 * @param rows: will be filled with number of rows
 * @param cols: will be filled with number of cols
 */
int load_matlab_file_into_memory(cl_float **memory, mxArray *mat, int *rows, int *cols);


/**
 * Saving matrix of type gsl into matlab file
 *
 * @param matrix: Matrix of type gsl to be saved
 * @param file_name: path to the matlab file (all directories must exist!)
 * @param variable_name: name of variable to be written in matlab file
 */
void save_matrix_to_file_gsl(gsl_matrix *matrix, char *file_name, char *variable_name);

/**
 * Saving matrix of type float into matlab file
 *
 * @param matrix: Matrix of type float to be saved
 * @param rows: number of rows of matrix
 * @param cols: number of cols of matrix
 * @param file_name: path to the matlab file (all directories must exist!)
 * @param variable_name: name of variable to be written in matlab file
 */
void save_matrix_to_file(float *matrix, int rows, int cols, char *file_name, char *variable_name);

#endif
#endif

#include "calibration_matrix.h"

#if defined(LIBXML_READER_ENABLED) && defined(LIBXML_PATTERN_ENABLED) && defined(LIBXML_OUTPUT_ENABLED)
#endif

static xmlDocPtr extractFile(const char *filename, const xmlChar *pattern) {
    xmlDocPtr doc;
    xmlTextReaderPtr reader;
    int ret;

    /*
     * build an xmlReader for that file
     */
    reader = xmlReaderForFile(filename, NULL, 0);
    if (reader != NULL) {
        /*
	 * add the pattern to preserve
	 */
        if (xmlTextReaderPreservePattern(reader, pattern, NULL) < 0) {
            fprintf(stderr, "%s : failed add preserve pattern %s\n",
	            filename, (const char *) pattern);
	}
	/*
	 * Parse and traverse the tree, collecting the nodes in the process
	 */
        ret = xmlTextReaderRead(reader);
        while (ret == 1) {
            ret = xmlTextReaderRead(reader);
        }
        if (ret != 0) {
            fprintf(stderr, "%s : failed to parse\n", filename);
	    xmlFreeTextReader(reader);
	    return NULL;
        }
	/*
	 * get the resulting nodes
	 */
	doc = xmlTextReaderCurrentDoc(reader);
	/*
	 * Free up the reader
	 */
        xmlFreeTextReader(reader);
    } else {
        fprintf(stderr, "Unable to open %s\n", filename);
	    return NULL;
    }
    return doc;
}

int fill_filter(xmlXPathContextPtr xpathCtx, float* filter, xmlChar* xpathExpr) {
    xmlXPathObjectPtr xpathObj;
    xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);
    if(xpathObj == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", xpathExpr);
        xmlXPathFreeContext(xpathCtx);
        xmlXPathFreeObject(xpathObj);
        return -1;
    }

    xmlNodeSetPtr nodes = xpathObj->nodesetval;
    xmlNodePtr cur;
    int size = (nodes) ? nodes->nodeNr : 0;

    for(int i = 0; i < size; ++i) {
        cur = nodes->nodeTab[i];
        char *cur_content = (char*)xmlNodeGetContent(cur);
        char *ch;
        ch = strtok(cur_content, " ");
        int counter = 0;

        while (ch != NULL) {
            filter[i*25 + counter] = atof(ch);
            ch = strtok(NULL, " ");
            counter++;
        }
    }




    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    return 0;
}

int create_calibration_matrix(char *filename_path, float *filter1, float *filter2) {
    const char *pattern = "correction_matrix";
    xmlDocPtr doc;
    xmlXPathContextPtr xpathCtx;

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

    doc = extractFile(filename_path, (const xmlChar *) pattern);
    if (doc != NULL) {

        xpathCtx = xmlXPathNewContext(doc);
        if(xpathCtx == NULL) {
            fprintf(stderr,"Error: unable to create new XPath context\n");
            xmlFreeDoc(doc);
            return -1;
        }
        char* expr = "//sensor_calibration/system_info/spectral_correction_info/correction_matrices/correction_matrix[1]/virtual_bands/virtual_band/coefficients";
        fill_filter(xpathCtx, filter1, (xmlChar *)expr);
        expr = "//sensor_calibration/system_info/spectral_correction_info/correction_matrices/correction_matrix[2]/virtual_bands/virtual_band/coefficients";
        fill_filter(xpathCtx, filter2, (xmlChar *)expr);

    }
    xmlXPathFreeContext(xpathCtx);
    xmlFreeDoc(doc);
    xmlCleanupParser();
    xmlMemoryDump();
}

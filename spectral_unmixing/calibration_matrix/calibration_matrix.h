#ifndef CALIB_MAT
#define CALIB_MAT

#include <stdio.h>
#include <string.h>
#include <libxml/xmlreader.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

/**
 * Creating filter for Ximea camera
 *
 * @param filename_path: path to the xml document with filter information (only official)
 * @param filter1: matrix that will be filled with short filter correction matrix
 * @param filter2: matrix that will be filled with long filter correction matrx
 */
int create_calibration_matrix(char *filename_path, float *filter1, float *filter2);

#endif

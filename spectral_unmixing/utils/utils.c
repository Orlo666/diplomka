#include "utils.h"
char *getKernelSource(char *filename)
{
    FILE *file = fopen(filename, "r");
    if (!file)
    {
        fprintf(stderr, "Error: Could not open kernel source file\n");
        exit(EXIT_FAILURE);
    }
    fseek(file, 0, SEEK_END);
    int len = ftell(file) + 1;
    rewind(file);

    char *source = (char *)calloc(sizeof(char), len);
    if (!source)
    {
        fprintf(stderr, "Error: Could not allocate memory for source string\n");
        exit(EXIT_FAILURE);
    }
    int read_err = fread(source, sizeof(char), len, file);
    if (read_err != len - 1) {
        fclose(file);
        fprintf(stderr, "ERROR: Could not read kernel source file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    fclose(file);
    return source;
}

int create_identity_matrix(cl_float *matrix, int cols, int rows) {
    if (cols != rows) {
        return 1;
    }
    for (int i=0; i<cols*rows; i++) {
        matrix[i] = 0.0;
    }
    for (int i=0; i<cols; i++) {
        matrix[i*cols+i] = 1.0;
    }
}




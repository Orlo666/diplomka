#ifndef TRANS_H
#define TRANS_H
#ifndef __MULT_HDR
#define __MULT_HDR

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <clBLAS.h>
#include "err_code.h"
#include "utils.h"


/**
 * Matrix transponation
 *
 * @param d_matrix: openCl buffer with matrix to transpose
 * @param d_out_matrix: openCl buffer that will be filled with transposed matrix
 * @param rows: number of rows in d_matrix
 * @param cols: number of cols in d_matrix
 * @param ctx: openCl context
 * @param device_id: openCl device
 */
int transpose(cl_mem d_matrix, cl_mem d_out_matrix, cl_int rows, cl_int cols, cl_context ctx, cl_device_id device_id);

#endif
#endif

__kernel void sum_to_one(
   __global float *abundances,
   const int abun_rows,
   const int abun_cols) {

    int idx = get_global_id(0);
    int gx = 0;
    if (idx < abun_cols) {
        double sum = 0.0;
        for (int i=0; i<abun_rows; i++) {
            if (abundances[abun_cols*i + idx] < 0.0) {
                abundances[abun_cols*i + idx] = .0;
            }
            sum += abundances[abun_cols*i + idx];
        }

        for (int i=0; i<abun_rows; i++) {
            abundances[abun_cols*i + idx] = (float)abundances[abun_cols*i + idx]/sum;
        }
    }
}

#ifndef PPI_H
#define PPI_H

#ifndef __MULT_HDR
#define __MULT_HDR

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <clBLAS.h>
#include <time.h>

#include <err_code.h>

/**
 * Computes Pixel purity index
 *
 * @param d_image: openCl buffer of original hyperspectral 2-d image with sizes (rows*cols)*bands
 * @param rows: number of bands of d_image
 * @param cols: number of rows*cols of d_image
 * @param d_endmembers: openCl buffer that will be filled with endmembers with sizes bands * endmembers
 * @param q: number of selected endmembers
 * @param d_skewers: openCl buffer with skewers sizes iter * bands
 * @param ctx: openCl context
 * @param device_id: openCl device

 */
int ppi(cl_mem d_image, cl_int rows, cl_int cols, cl_mem d_endmembers, cl_int q, cl_mem d_skewers, cl_int iter,
        cl_context ctx, cl_device_id device_id);

/**
 * Generating skewers, random float numbers
 *
 * @param skewers: openCl buffer that will be filled with skewers
 * @param rows: number of rows for skewers
 * @param cols: number of cols for skewers
 * @param ctx: openCl context
 * @param device_id: openCl device
 */
int generate_random_skewers(cl_mem skewers, cl_int rows, cl_int cols,
    cl_context ctx, cl_device_id device_id);

#endif
#endif

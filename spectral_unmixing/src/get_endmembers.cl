__kernel void get_endmembers(
    __global float *image,
    __global float *end_members,
    __global int *end_members_idx,
    const int rows,
    const int cols,
    const int q
) {
    int g_idx = get_global_id(0);
    int g_idy = get_global_id(1);

    if (g_idx < rows){
        end_members[g_idx*q + g_idy] = image[g_idx*cols + end_members_idx[g_idy]];
    }
}

#include <fcls.h>

#ifndef DEVICE
#define DEVICE CL_DEVICE_TYPE_DEFAULT
#endif

#define MAX_ENDMEMBERS 35 //due to isra kernel

extern int output_device_info(cl_device_id );
extern char *getKernelSource(char *filename);

void inverse(cl_float* A, int N)
{
    int *IPIV = malloc(N * sizeof(int));
    int LWORK = N*N;
    cl_float *WORK = malloc(LWORK * sizeof(int));
    int INFO;

    sgetrf_(&N,&N,A,&N,IPIV,&INFO);
    sgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);

    free(IPIV);
    free(WORK);
}

int non_constrained(cl_mem d_abundances, cl_mem d_image, cl_int rows, cl_int cols, cl_mem d_endmembers, cl_int end_rows, cl_int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id) {
    clblasOrder order = clblasRowMajor;
    cl_int err;
    cl_mem d_em_em_i;
    cl_mem d_em_em_em;
    cl_event event = NULL;
    cl_float *em_em_result = malloc(sizeof(cl_float) * end_cols * end_cols);
    cl_float *em_em_em_result = malloc(sizeof(cl_float) * end_cols * end_cols);
    int ret = 0;

    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);

    /*********************
     ****** EM' * EM ******
     *********************/
    d_em_em_i = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_cols * end_cols * sizeof(cl_float),
                          NULL, &err);
    checkError(err, "Buffer d_em_em_i");

    err = clblasSgemm(order, clblasTrans, clblasNoTrans, end_cols, end_cols, end_rows,
                      1.0, d_endmembers, 0, end_cols,
                      d_endmembers, 0, end_cols,
                      0.0, d_em_em, 0, end_cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        /* Fetch results of calculations from GPU memory. */
        checkError(err, "Calculating EM * EM");
        err = clEnqueueReadBuffer(queue, d_em_em, CL_TRUE, 0, end_cols * end_cols * sizeof(cl_float),
                                  em_em_result, 0, NULL, NULL);
        checkError(err, "Reading em_em");
        err = clFinish(queue);
        checkError(err, "Finishing reading em_em");
    }

    /*****************************************
     ****** INVERSION OF em_em_result ********
     *****************************************/
    inverse(em_em_result, (long int)end_cols);

    err = clEnqueueWriteBuffer(queue, d_em_em_i, CL_TRUE, 0,
                               end_cols * end_cols * sizeof(cl_float), em_em_result, 0, NULL, NULL);
    checkError(err, "Writing inverse back to d_em_em");

    clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    /*****************************************
     ****** I(EM' * EM) * EM' ****************
     *****************************************/

    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }
    checkError(err, "creating queue");


    event = NULL;

    d_em_em_em = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_rows * end_cols * sizeof(cl_float),
                          NULL, &err);
    checkError(err, "creating buffer d_em_em_em");
    err = clEnqueueWriteBuffer(queue, d_em_em_em, CL_TRUE, 0,
                               end_rows * end_cols * sizeof(cl_float), em_em_em_result, 0, NULL, NULL);
    checkError(err, "enqueing d_em_em_em");

    err = clblasSgemm(order, clblasNoTrans, clblasTrans, end_cols, end_rows, end_cols,
                      1.0, d_em_em_i, 0, end_cols,
                      d_endmembers, 0, end_cols,
                      0.0, d_em_em_em, 0, end_rows, 1, &queue, 0, NULL, &event);
    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        checkError(err, "returning result of em_em_i * em");
    }

    clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    queue = clCreateCommandQueue(ctx, device_id, 0, &err);

    /*****************************************
     ************ EM_EM_EM * IMAGE ***********
     *****************************************/
    err = clblasSgemm(order, clblasNoTrans, clblasNoTrans, end_cols, cols, rows,
                      1.0, d_em_em_em, 0, end_rows,
                      d_image, 0, cols,
                      0.0, d_abundances, 0, cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        checkError(err, "returning result of em_em_em * image");
    }

    clReleaseEvent(event);
    clReleaseMemObject(d_em_em_i);
    clReleaseMemObject(d_em_em_em);
    clReleaseCommandQueue(queue);
    free(em_em_result);
    free(em_em_em_result);
    return 0;
}


int isra(cl_mem d_abundances, cl_mem d_image, cl_int rows, cl_int cols, cl_mem d_endmembers, cl_int end_rows, cl_int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id) {
    if (end_cols > MAX_ENDMEMBERS) {
        printf("Maximum number of possible selected endmembers is %d\n", MAX_ENDMEMBERS);
        return 1;
    }
    cl_int err;
    size_t max_threads = 1024;

    cl_command_queue queue = 0;
    cl_event event = NULL;
    cl_int iter = 1000;
    cl_program program;
    cl_kernel kernel;
    clblasOrder order = clblasRowMajor;
    cl_mem d_em_x;
    int ret = 0;

    queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating queue");

    /*********************
     ****** EM' * X ******
     *********************/
    d_em_x = clCreateBuffer(ctx, CL_MEM_READ_WRITE, end_cols * cols * sizeof(cl_float),
                          NULL, &err);
    checkError(err, "Creating buffer d_em_x");

    err = clblasSgemm(order, clblasTrans, clblasNoTrans, end_cols, cols, end_rows,
                      1.0, d_endmembers, 0, end_cols,
                      d_image, 0, cols,
                      0.0, d_em_x, 0, cols, 1, &queue, 0, NULL, &event);

    if (err != CL_SUCCESS) {
        printf("clblasDgemv() failed with %d\n", err);
        ret = 1;
        return ret;
    }
    else {
        /* Wait for calculations to be finished. */
        err = clWaitForEvents(1, &event);
        checkError(err, "returning for EM * x");
    }
    char *isra_kernel = getKernelSource("src/isra.cl");

    /* Setup OpenCL environment. */
    if (err != CL_SUCCESS) {
        printf( "clCreateCommandQueue() failed with %d\n", err );
        clReleaseContext(ctx);
        return 1;
    }

    /*********************
    ****** ISRA **********
    **********************/
    program = clCreateProgramWithSource(ctx, 1, (const char **) & isra_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "isra", &err);
    checkError(err, "Creating kernel");
    err = clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &max_threads, NULL);
    checkError(err, "Check a max number of threads");

    size_t global;
    int mod = cols % max_threads;
    size_t global_isra = cols;
    if (mod > 0) {
        global_isra += max_threads - mod;
    }
    const size_t local_isra = max_threads;

    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_image);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_abundances);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_endmembers);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_em_em);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_em_x);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 5, sizeof(cols), &cols);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 6, sizeof(cl_int), &rows);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 7, sizeof(cl_int), &end_cols);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 8, sizeof(cl_int), &end_rows);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 9, sizeof(cl_int), &iter);
    checkError(err, "Setting kernel arguments");
    err |= clSetKernelArg(kernel, 10, sizeof(cl_float) * end_cols * end_cols, NULL);
    checkError(err, "Setting kernel arguments");

    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, 0,
        &global_isra, &local_isra,
        0, NULL, NULL);
    checkError(err, "Enqueing Range Kernel");
    err = clFinish(queue);
    checkError(err, "Finishing queue");

    clReleaseEvent(event);
    clReleaseCommandQueue(queue);
    err = clReleaseKernel(kernel);

    clReleaseMemObject(d_em_x);
    free(isra_kernel);

    return 0;
}

int sum_to_one(cl_mem d_abundances, cl_int abun_rows, cl_int abun_cols, cl_context ctx, cl_device_id device_id) {
    cl_int err;
    cl_event event = NULL;
    cl_program program;
    size_t max_threads = 1024;
    int ret = 0;
    cl_kernel kernel;

    char *sum_to_one_kernel = getKernelSource("src/sum_to_one.cl");
    cl_command_queue queue = clCreateCommandQueue(ctx, device_id, 0, &err);
    checkError(err, "Creating queue");

    /* Setup OpenCL environment. */
    program = clCreateProgramWithSource(ctx, 1, (const char **) & sum_to_one_kernel, NULL, &err);
    checkError(err, "Creating program");
    err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];
        printf("Error: Failed to build program executable!\n%s\n", err_code(err));
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        return EXIT_FAILURE;
    }
    kernel = clCreateKernel(program, "sum_to_one", &err);
    checkError(err, "Creating kernel");

    err = clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &max_threads, NULL);
    checkError(err, "Check a max number of threads");


    int mod = abun_cols % max_threads;
    size_t global_sum = abun_cols;
    if (mod > 0) {
        global_sum += max_threads - mod;
    }
    const size_t local_sum = max_threads;
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_abundances);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 1, sizeof(cl_int), &abun_rows);
    checkError(err, "Setting kernel arguments");
    err = clSetKernelArg(kernel, 2, sizeof(cl_int), &abun_cols);
    checkError(err, "Setting kernel arguments");

    err = clEnqueueNDRangeKernel(
        queue,
        kernel,
        1, NULL,
        &global_sum, &local_sum,
        0, NULL, NULL);

    checkError(err, "Enqueueing kernel");
    err = clFinish(queue);
    checkError(err, "Finishing queue");
    err = clReleaseProgram(program);
    err = clReleaseKernel(kernel);
    return 0;
}



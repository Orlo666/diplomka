#ifndef NORM_H
#define NORM_H
#ifndef __MULT_HDR
#define __MULT_HDR

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <clBLAS.h>
#include "err_code.h"
#include "utils.h"

/**
 * unit vector normalization of matrix by columns or by row
 *
 * @param d_matrix: openCl buffer with matrix to normalize
 * @param rows: number of rows in d_matrix
 * @param cols: number of cols in d_matrix
 * @param @by_band: switch that will turn normalization by row, 0-no, 1-yes
 * @param ctx: openCl context
 * @param device_id: openCl device
 */
int normalize_matrix(cl_mem d_matrix, cl_int rows, cl_int cols, cl_int by_band, cl_context ctx, cl_device_id device_id);

#endif
#endif

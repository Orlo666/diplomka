__kernel void max_indexes(
    __global float *vector,
    const int vector_size,
    const int cols,
    __global int *idx) {

    int k =0;
    int gid_x = get_global_id(0);
    float max = -1.0;
    for (k=0; k<vector_size; k++) {
        if (fabs(vector[gid_x + k*cols]) > max) {
            max = fabs(vector[gid_x + k*cols]);
            idx[gid_x] = k;
        }
    }
}

__kernel void max_indexes_reduction(
    __global float *vector,
    __global int *idx,
    const int vector_size,
    const int cols,
    const int chunk,
    const int first_run) {

    int k =0;
    int gid_x = get_global_id(0);
    int gid_y = get_global_id(1);
    int lid_x = get_local_id(0);
    int lid_y = get_local_id(1);
    int gr_y = get_group_id(1);

    if (gid_y < cols) {
        float max = -1.0;
        int pos = 0;
        float tmp;
        for (k=lid_x*chunk; k<lid_x*chunk + chunk; k++) {

            if (first_run) {
                tmp = fabs(vector[k*cols + gid_y]);
            } else {
                tmp = fabs(vector[idx[k*cols + gid_y]*cols + gid_y]);
            }
            if (k < vector_size && tmp > max) {
                max = tmp;
                pos = k;
            }
        }
        if (!first_run) {
            pos = idx[pos*cols + gid_y];
        }
        idx[lid_x*cols + gid_y] = pos;
    }
}


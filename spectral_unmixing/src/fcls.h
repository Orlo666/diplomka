#ifndef FCLS_H
#define FCLS_H
#ifndef __MULT_HDR
#define __MULT_HDR

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <math.h>
#include <string.h>
#include <clBLAS.h>
#include <time.h>
#include <lapacke.h>

#include "err_code.h"
#include "utils.h"


/**
 * Computes unconstrained abundances
 * (E'*E)*E'
 *
 * @param d_abundances: openCl buffer in which abundances will be stored
 * @param d_image: openCl buffer of original hyperspectral 2-d image with sizes (rows*cols)*bands
 * @param rows: number of bands of d_image
 * @param cols: number of rows*cols of d_image
 * @param d_endmembers: openCl buffer with selected endmembers as 2-d matrix with sizes bands * endmembers
 * @param end_rows: number of bands of d_image
 * @param end_cols: number of selected endmembers
 * @param d_em_em: openCl buffer which will be filled for later processing with sizes endmembers * endmembers
 * @param ctx: openCl context
 * @param device_id: openCl device
 */
int non_constrained(cl_mem d_abundances, cl_mem d_image, cl_int rows, cl_int cols, cl_mem d_endmembers, cl_int end_rows, cl_int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id);

/**
 * Computes ISRA algorithm
 * PHI^(k+1) = PHI^k * ((E'*x) / (E'*E*PHI^k))
 *
 * @param d_abundances: openCl buffer in which abundances will be stored
 * @param d_image: openCl buffer of original hyperspectral 2-d image with sizes (rows*cols)*bands
 * @param rows: number of bands of d_image
 * @param cols: number of rows*cols of d_image
 * @param d_endmembers: openCl buffer with selected endmembers as 2-d matrix with sizes bands * endmembers
 * @param end_rows: number of bands of d_image
 * @param end_cols: number of selected endmembers
 * @param d_em_em: openCl buffer sizes endmembers * endmembers, used for optimalization
 * @param ctx: openCl context
 * @param device_id: openCl device
 */

int isra(cl_mem d_abundances, cl_mem d_image, cl_int rows, cl_int cols, cl_mem d_endmembers, cl_int end_rows, cl_int end_cols, cl_mem d_em_em, cl_context ctx, cl_device_id device_id);

/**
 * Summing abundance values to one and cutting off negative values
 * @param d_abundances: openCl buffer in which abundances will be stored
 * @param abun_rows: number of selected endmembers
 * @param abun_cols: number of rows*cols of d_image
 * @param ctx: openCl context
 * @param device_id: openCl device
 */
int sum_to_one(cl_mem d_abundances, cl_int abun_cols, cl_int abun_rows, cl_context ctx, cl_device_id device_id);

#endif
#endif

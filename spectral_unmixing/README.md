# Project Title

Spectral unmixing

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
MATLAB 2016b
sudo apt install ocl-icd-opencl-dev
sudo apt install libgsl0-dev
sudo apt install libxml2-dev
sudo apt install libblas-dev liblapack-dev liblapacke-dev
download [clBLAS release] (https://github.com/clMathLibraries/clBLAS/releases)  into main directory of project and link clBLAS.so.2 in /url/lib. Otherwise appropriate changes in Makefile are required
```

if another version of MATLAB is installed, please change Makefile according to the changes

### Installing

```
cd spectral_unmixing
make
```

### How to run
input data must be 3d matlab array with sizes of Height x Width x Bands
or 2d array with sizes (Height * Width) x Bands
```
./process --input=<input_mat_file> --variable=<variable_in_input_mat_file> --output=<output_mat_file>  <other_options>
```

## Options with arguments
```
endmembers=<number_of_endmembers_to_select> - default 3
output_variable=<name_of_output_variable>
```
## Options without arguments
```
normalize - normalize by UVN
normalize_band - normalie by UVN by row
filter_short - use correction for short filter
filter_long - use correction for long filter
```

## Built With

* make


## Authors

* **Roman Orlicek**




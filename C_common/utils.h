#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err_code.h>

#define MAX_PLATFORMS     8
#define MAX_DEVICES      16
#define MAX_INFO_STRING 256


char *getKernelSource(char *filename);
int create_identity_matrix(cl_float *matrix, int cols, int rows);
